﻿/// <reference path="D:\tfs\ModuloFUEC\TE_FUEC\Dev_20170508\Satrack.FUEC\Satrack.FUEC.WebUI\templates/modals/modalError.html" />
'use strict';

// Controlador principal
app.controller('generalController', ['$scope', '$mdDialog', 'clientCpt', 'urlWeb', '$mdToast', '$location', function ($scope, $mdDialog, clientCpt, urlWeb, $mdToast, $location) {

    //$scope.Token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoiamFsZW9uIiwibmJmIjoxNTIxNDUyMTAxLCJpYXQiOjE1MDU4MTM3MDEsImV4cCI6MTUwNTg1NjkwMSwiY2FsbFVybCI6IiAiLCJjbGllbnRJZCI6Mzc1MCwidXN1YXJpb2NyZWFkb3IiOiJ1bmRlcnRha2VyIiwicmF6b25zb2NpYWwiOiI4MzAwNjk1OTkiLCJyb2xlcyI6WyJvcGVyYWRvckNvbWFuZG9zIiwiYWRtaW4iXSwiaWRQYWlzIjoxLCJSZWZlcmVuY2lhU2VydmljaW9zIjoiMTU3RkZEIiwiUmVmZXJlbmNpYUFwbGljYWNpb25lcyI6IjdGRSIsIlRpbWVab25lIjoiIiwiTWVhc3VyZSI6IiJ9.j_Z4ngUzDF-dTVZXLQvUJD4vPIwH_KlkEaJT3pxSbjQ';//$location.search().JWT;
    //$scope.Token = $location.search().JWT;
    //// IDCliente
    //$scope.ClientID = $location.search().ClientID;
    //$scope.UserPage = $location.search().User;
    $scope.RoleIDPage = 0;

    $scope.UrlsMenu = [];
    $scope.loaderMain = true;

    var FirstPage = {};

    // Inicializamos el proceso del menú
    loadMenu($scope, clientCpt, urlWeb);

    // Inicializamos los modals(error, success, warning, info)
    loadMessage($scope, $mdDialog, $mdToast);
    
    function loadMenu() {

        //Inicia la primera pagina cargada del objeto
            $scope.UrlsMenu = ViewMenu(1)
            FirstPage = $scope.UrlsMenu[0];
            $scope.currentNavItem = FirstPage.Name;
            $scope.urlPage = FirstPage.Url;
            $scope.loaderMain = false;
            //$scope.RoleIDPage = data.RoleID;
        

        $scope.gcGoto = function (url) {
            $scope.urlPage = url;
        };
    }

    function ViewMenu(RoleID)
    {
        var menu = [];

        switch(RoleID) {
            case 1:
                AdminView(menu); 
                break;           
            default:
                DefaultView(menu);
                break;
        }

        return menu;
    }

    function AdminView(menu)
    {
        var urlMain = { Url: urlWeb.main.metPageMain, Tittle: "Inicio", Name: "main" };

        var urlClient = { Url: urlWeb.client.metPageClient, Tittle: "Clientes", Name: "client", Image: "images/Conductores.png", Description: "Administre y configure la información de sus clientes." };
        
        menu.push(urlMain);
        menu.push(urlClient);
    }

    function DefaultView(menu)
    {
        var defaultView = { Url: urlWeb.errorPage.metPageError, Tittle: "CONTROL DE ACCESO", Name: "error" };
        menu.push(defaultView) ;
    }

}]);

// Función para cargar las URL del menú


//Funcion para formato generico de fechas
function FormatDate(ddMMyyy) {
    var s = ddMMyyy + " 00:00";
    var bits = s.split(/\D/);
    var date = new Date(bits[2], --bits[1], bits[0], bits[3], bits[4]);

    return date;
}

// Función para cargar los modals
function loadMessage($scope, $mdDialog, $mdToast) {
    // Estructura modal general reutilizable
    // ---- ---- ---- MODALS
    // Funcion para presentar el modal
    $scope.gcOpenAlertImage = function (arrayMessages) { // --> modal respuesta correcta imagen
        $mdDialog.show({
            skipHide: true,
            controller: DialogController,
            templateUrl: 'templates/modals/modalImage.html',
            parent: angular.element(document.body),
            locals: {
                message: arrayMessages
            }
        })
    };

    $scope.gcOpenAlert = function (arrayMessages) { // --> modal respuesta erronea
        $mdDialog.show({
            skipHide: true,
            controller: DialogController,
            templateUrl: 'templates/modals/modalAlert.html',
            parent: angular.element(document.body),
            locals: {
                message: arrayMessages
            }
        })
    };

    $scope.gcOpenToastSuccess = function (arrayMessages) { // --> modal respuesta correcta
        $mdToast.show(
          $mdToast.simple()
            .parent(angular.element(document.body))
            .textContent(arrayMessages[0])
            .position('top right')
            .hideDelay(3000)
        )
    };

    $scope.gcOpenModalLoad = function () { // --> modal cargando...
        $mdDialog.show({
            template: '<md-dialog id="plz_wait" style="box-shadow:none">' +
              '<md-dialog-content style="overflow:hidden;" layout="row" layout-margin layout-padding layout-align="center center" aria-label="wait">' +
              '<md-progress-circular md-mode="indeterminate" md-diameter="50"></md-progress-circular>' +
              'CARGANDO...' +
              '</md-dialog-content>' +
              '</md-dialog>',
            parent: angular.element(document.body),
            clickOutsideToClose: false,
            fullscreen: false,
            escapeToClose: false
        });
    };

    $scope.gcCloseModalLoad = function () {
        $mdDialog.cancel();
    };

    // Métodos del modal
    function DialogController($scope, $mdDialog, message) {
        $scope.message = message;
        $scope.hide = function () {
            $mdDialog.hide();
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        };
    }
    // ---- ---- ---- MODALS
}