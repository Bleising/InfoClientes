﻿'use strict';

// Directivas utilizadas en toda la aplicación
app.directive('numbersLettersOnly', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ctrl) {
            var validateNumber = function (inputValue) {
                if (inputValue === undefined) {
                    return '';
                }
                var transformedInput = inputValue.replace(/[^A-Za-zÑñáéíóúÁÉÍÓÚ0-9-_\s]/g, '');
                if (transformedInput !== inputValue) {
                    ctrl.$setViewValue(transformedInput);
                    ctrl.$render();
                }
                
                return transformedInput;
            };

            ctrl.$parsers.unshift(validateNumber);
            ctrl.$parsers.push(validateNumber);
        }
    };
});

// Directiva para exportar objetos a excel
app.directive('excelExport',
    function () {
        return {
            restrict: 'A',
            scope: {
                fileName: "@",
                dataHead: "&exportHeadData",
                data: "&exportData"
            },
            replace: true,
            template: '<button class="btn btn-primary btn-ef btn-ef-3 btn-ef-3c mb-10" ng-click="download()">Export to Excel <i class="fa fa-download"></i></button>',
            link: function (scope, element) {
                scope.download = function () {
                    var blob = new Blob([getSheet(scope.dataHead(), scope.data())], {
                        type: "application/vnd.ms-excel;charset=utf-8"
                    });
                    saveAs(blob, scope.fileName + ".xls");

                    // Creación del template de la tabla 
                    function getSheet(dataHead, data) {
                        var exportTable = "<table>";
                        exportTable += "<thead><tr>";
                        for (var R = 0; R != dataHead.length; ++R) {
                            for (var C = 0; C != dataHead[R].length; ++C) {
                                exportTable += "<th style='background-color: #253081; color: white; width: 200px; height: 40px; vertical-align: middle'>" + dataHead[R][C] + "</th>";
                            }
                        }
                        exportTable += "</tr></thead>";

                        exportTable += "<tbody>";
                        for (var R = 0; R != data.length; ++R) {
                            exportTable += "<tr>";
                            for (var C = 0; C != data[R].length; ++C) {
                                exportTable += "<td style='text-align: center; border: 1px solid #eee; height: 40px; vertical-align: middle'>" + data[R][C] + "</td>";
                            }
                            exportTable += "</tr>";
                        }
                        exportTable += "</tbody>";
                        exportTable += "</table>";

                        return exportTable;
                    };
                };
            }
        };
    }
 );