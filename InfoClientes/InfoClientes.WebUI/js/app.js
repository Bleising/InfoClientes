﻿// Definición de librerías a utilizar, claro, esta, adaptadas a angular
var app = angular.module('appInfoClientes', ['dataGrid', 'pagination', 'ngRoute', 'ngMaterial', 'ngMessages', 'base64']);

// Variables constantes y globales
// Esqueleto del esquema de URL de los metodos del WEB API
//http://localhost:51687/SatrackFUECWebAPI
var serverLocation = "http://prijuanor/InfoClientesWebAPI";
//var serverLocation = "http://localhost:51687";
//var serverLocation = "http://pripruebas/SatrackFUECWebAPI";
//var serverLocation = "http://172.16.1.249/SatrackFUECWebAPI";
app.constant("urlApi", {
            "client": {
                "metGetclient": serverLocation + "/api/Client/GetAllClients",
                "metPostSaveOrUpdateClient": serverLocation + "/api/Client/PostSaveOrUpdateClient",
            },
            "masterLocation": {
                "metGetCities": serverLocation + "/api/LocationAndMaster/GetAllCities/?departamentID=",
                "metGetDepartaments": serverLocation + "/api/LocationAndMaster/GetAllDepartaments?paisID=",
                "metGetCountries": serverLocation + "/api/LocationAndMaster/GetAllCountries"
            }
        }
);

app.constant("urlWeb", {
                "client": {
                    "metPageClient":  "components/client/client.view.html"
                },
                "defaultView": {
                    "metPageDefault": "components/main/main.view.html"
                },
                "errorPage": {
                    "metPageError": "components/error/accessError.view.html"
                },
                "main": {
                    "metPageMain": "components/main/main.view.html"
                }
});
