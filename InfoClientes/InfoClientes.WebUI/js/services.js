﻿// Se usa este comando para validar que la sintaxis se esta haciendo de la forma correcta
'use strict';

// Servicio del componente contratante
app.factory('clientCpt', function ($http, $q, $httpParamSerializerJQLike, urlApi, $filter) {
    return {

        getUserPermission: function (user, token) {
            var deferred = $q.defer();
            //$http.defaults.headers.common['Authorization'] = token;
            $http({
                method: 'POST',
                dataType: "JSON",
                contentType: 'application/json',
                url: urlApi.usersRole.metGetUserPermission,
                data: JSON.stringify(user),//$httpParamSerializerJQLike(user),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': token
                },
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                // when the response is available

                deferred.resolve(response.data);

            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                deferred.resolve()
            });
            return deferred.promise;
        },

    }
});