﻿// control de rutas
app.config(['$locationProvider', '$routeProvider', '$httpProvider', '$mdDateLocaleProvider', '$compileProvider', function ($locationProvider, $routeProvider, $httpProvider, $mdDateLocaleProvider, $compileProvider) {

    // Rutas amigables del aplicativo
    $routeProvider
    .when("/", { templateUrl: "master_page.html", controller: "generalController" })
    .otherwise({ redirectTo: "404" });

    //$locationProvider.html5Mode(true);
    // Header de las peticiones a las APIS
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

    // Ruta HREF
    $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|sms|tel):/);

    // Formato, idioma y cofiguracion del datapicker 
    $mdDateLocaleProvider.formatDate = function (date) {
        return date ? moment(date).format('DD/MM/YYYY') : null;
    };
    $mdDateLocaleProvider.parseDate = function (dateString) {
        var m = moment(dateString, 'DD/MM/YYYY', true);
        return m.isValid() ? m.toDate() : new Date(NaN);
    };
    $mdDateLocaleProvider.months =
        ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
    $mdDateLocaleProvider.shortMonths =
        ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'];
    $mdDateLocaleProvider.days =
        ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sábado'];
    $mdDateLocaleProvider.shortDays =
        ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'S\u00E1'];
}]);