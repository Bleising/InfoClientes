﻿// Se usa este comando para validar que la sintaxis se esta haciendo de la forma correcta
'use strict';

// Servicio del componente contratante
app.factory('certificateServiceCpt', function ($http, $q, $httpParamSerializerJQLike, urlApi, $filter) {
    return {
        getContractorsListCert: function (ClientID, token) {

            var deferred = $q.defer();
            $http({
                method: 'GET',
                dataType: "JSON",
                url: urlApi.Contractor.metGetContractors + ClientID,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': token
                },
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                // when the response is available

                //response.data = mockTest;
                deferred.resolve(response.data);

            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                deferred.resolve()
            });

            //deferred.resolve(mockData);
            return deferred.promise;
        },

        getContractObjects: function (ClientID, token) {
            var deferred = $q.defer();
            $http({
                method: 'GET',
                dataType: "JSON",
                url: urlApi.company.metGetContractObjects + ClientID,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': token
                },
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                // when the response is available
                deferred.resolve(response.data);
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                deferred.resolve()
            });
            return deferred.promise;
        },

        getStates: function (token) {
            var deferred = $q.defer();
            $http({
                method: 'GET',
                dataType: "JSON",
                url: urlApi.State.metGetStates,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': token
                },
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                // when the response is available
                deferred.resolve(response.data);
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                deferred.resolve()
            });
            return deferred.promise;
        },

        getCities: function (StateID, token) {
            var deferred = $q.defer();
            $http({
                method: 'GET',
                dataType: "JSON",
                url: urlApi.City.metGetCities + StateID,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': token
                },
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                // when the response is available
                deferred.resolve(response.data);
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                deferred.resolve()
            });
            return deferred.promise;
        },
        getVehicle: function (ClientID, Plate, token) {
            var deferred = $q.defer();
            $http({
                method: 'GET',
                dataType: "JSON",
                url: urlApi.vehicle.metGetVehicle + Plate,
                headers: {
                'Content-Type': 'application/json',
                'Authorization': token
            },
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                // when the response is available
                deferred.resolve(response.data);
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                deferred.resolve()
            });

            return deferred.promise;
        },
        getVehiclesAutoCom: function (ClientID, token) {
            var deferred = $q.defer();
            $http({
                method: 'GET',
                dataType: "JSON",
                url: urlApi.vehicle.metGetPlatesForFUEC + ClientID,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': token
                },
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                // when the response is available
                deferred.resolve(response.data);
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                deferred.resolve()
            });

            return deferred.promise;
        },
        getDriversCertificate: function (ClientID, token) {
            var deferred = $q.defer();
            $http({
                method: 'GET',
                dataType: "JSON",
                url: urlApi.driver.metGetAllDrivers + ClientID,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': token
                },
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                // when the response is available
                deferred.resolve(response.data);
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                deferred.resolve()
            });
            return deferred.promise;
        },
        getCompanyCertificate: function (ClientID, token) {
            var deferred = $q.defer();
            $http({
                method: 'GET',
                dataType: "JSON",
                url: urlApi.company.metGetCompany + ClientID,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': token
                },
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                // when the response is available
                deferred.resolve(response.data);
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                deferred.resolve()
            });
            return deferred.promise;
        },
        sendMail: function (Driver, token) {
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: urlApi.Email.metSendMail,
                dataType: "json",
                contentType: "application/json charset=utf-8",
                data: JSON.stringify(Driver),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': token
                },
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                deferred.resolve(response.data);
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                deferred.resolve()
            });
            return deferred.promise;
        },
        getConsecutive: function (consecutiveData, token) {
            var deferred = $q.defer();
            $http({
                method: 'POST',
                dataType: "JSON",
                contentType: "application/json charset=utf-8",
                url: urlApi.certificate.metGetConsecutive,
                data: JSON.stringify(consecutiveData),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': token
                },
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                deferred.resolve(response.data);
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                deferred.resolve()
            });
            return deferred.promise;
        },
        SaveCertificate: function (Certificate, token) {
            var deferred = $q.defer();
            $http({
                method: 'POST',
                dataType: "JSON",
                contentType: "application/json charset=utf-8",
                url: urlApi.certificate.metPostSaveCertificate,
                data: JSON.stringify(Certificate),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': token
                },
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                deferred.resolve(response.data);
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                deferred.resolve()
            });
            return deferred.promise;
        },
        UseRegister: function (UserInfo, token) {
            var deferred = $q.defer();
            $http({
                method: 'POST',
                dataType: "JSON",
                contentType: "application/json charset=utf-8",
                url: urlApi.useRegister.metUseRegister,
                data: JSON.stringify(UserInfo),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': token
                },
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                deferred.resolve(response.data);
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                deferred.resolve()
            });
            return deferred.promise;
        }
    };
});
