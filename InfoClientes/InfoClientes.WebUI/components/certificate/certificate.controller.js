﻿//Se usa este comando para validar que la sintaxis se esta haciendo de la forma correcta
'use strict';

// Controlador del componente Certificado
app.controller("certificateControllerCpt", ['$scope', 'certificateServiceCpt', '$mdDialog', '$q', '$timeout', '$filter', '$sce', function ($scope, certificateServiceCpt, $mdDialog, $q, $timeout, $filter, $sce) {
    var today = new Date();
    var dateGenerate = today.getFullYear() + '_' + (today.getMonth()+1) + '_' + today.getDate();

    $scope.minDate = new Date(today.getFullYear(), today.getMonth(), today.getDate());
    $scope.minEndDate = new Date(today.getFullYear(), today.getMonth(), today.getDate());
    $scope.maxDate = new Date(today.getFullYear(), today.getMonth(), today.getDate());
    $scope.maxDateInitial = new Date(today.getFullYear(), today.getMonth(), today.getDate());
    $scope.arrContractors = { data: [] }; //= loadAll()
    $scope.ContractObjects = [];
    $scope.StateList = [];
    $scope.CityList = [];
    $scope.TextDestination = "";
    $scope.arrayVehicles = [];
    $scope.arrDrivers = { data: [] };
    $scope.itemDriverCert = {};
    $scope.gridDriverCert = { data: [] };
    $scope.ShowMessageVehicle = false;
    $scope.simulateQuery = true;
    $scope.objCertificate = { Destination: [], DriversList: [], ActualDate: new Date() };
    $scope.objCertificate.DisableCityList = true;
    $scope.objCertificate.DisableDestination = false;
    $scope.objCertificate.DisableDriver = false;
    $scope.objCertificate.DisableInitialDate = true;
    $scope.objCertificate.DisableEndDate = true;
    $scope.objCertificate.objMail = {};
    $scope.AlfanumericValidateCaracter = new RegExp("^[-_\\w\\.\\(\\)\\#\\,\\wÜüáéíóúÁÉÍÓÚ\\/\\ñÑ\\s]+$", "i");
    $scope.clientEmailAux = "";
    $scope.sendMailFlat = false;
    $scope.previewDocument = 1;
    
    // <<< Inicializacion de funciones y metodos >>>
    InitCertificate();

    // <<<<<<<<<<<<<<<<<<<<<< Funciones que deben registrarse en el $scope >>>>>>>>>>>>>>>>>>>>>>

    // Agrega efecto de espera al autocompletado de contratante
    $scope.querySearchCertificate = function (searchText) {
        var results = searchText != "" ? $scope.arrContractors.filter(createFilterFor(searchText)) : $scope.arrContractors,
          deferred;
        if ($scope.simulateQuery) {
            deferred = $q.defer();
            $timeout(function () { deferred.resolve(results); }, Math.random() * 1000, false);
            return deferred.promise;
        } else {
            return results;
        }
    }

    // Agrega efecto de espera al autocompletado de vehículos
    $scope.querySearchVehicle = function (searchTextVehicles) {
        var plates = searchTextVehicles != "" ? $scope.arrayVehicles.filter(function (plate) {
            return (plate.toUpperCase().indexOf(searchTextVehicles.toUpperCase()) !== -1);
        }) : $scope.arrayVehicles, deferred;
        deferred = $q.defer();
        $timeout(function () {
            deferred.resolve(plates);
        }, Math.random() * 1000, false);

        return deferred.promise;
    }

    // Agrega efecto de espera al autocompletado de conductores
    $scope.querySearchDriver = function (searchTextDriver) {
        var plates = searchTextDriver != "" ? $scope.arrDrivers.filter(function (plate) {
            return (plate.Name.toUpperCase().indexOf(searchTextDriver.toUpperCase()) !== -1);
        }) : $scope.arrDrivers, deferred;
        deferred = $q.defer();
        $timeout(function () {
            deferred.resolve(plates);
        }, Math.random() * 1000, false);

        return deferred.promise;
    }

    // Asigna el valor seleccionado del autocompletar al ng model encargado de obtener la info del contratante
    $scope.setModelCertificateItem = function (item) {
        if (item != null) {
            $scope.objCertificate.objContractor = item;
        } else {
            CleanContractor();
        }
    }

    // Asigna el valor seleccionado del autocompletar al ng model encargado de obtener la info del vehìculo
    $scope.setModelVehicle = function (item) {
        if (item != null) {
            getVehicle($scope.ClientID, item);
        } else {
            CleanVehicle();
        }
    }

    // Asigna el valor seleccionado del autocompletar al ng model encargado de obtener la info del conductor
    $scope.setModelDriver = function (item) {
        if (item != null) {
            $scope.itemDriverCert = item;
        } else {
            CleanDriver();
        }
    }

    //obtiene la lista de ciudades según el estado seleccionado
    $scope.getCitiesList = function () {
        $scope.objCertificate.CityValue = null;
        $scope.objCertificate.DisableCityList = false;
        $scope.objCertificate.objState = angular.fromJson($scope.objCertificate.StateValue);
        getCities($scope.objCertificate.objState.StateCode);
    }

    // Agrega los destinos a una lista
    $scope.addDestination = function () {
        if ($scope.TextDestination != "") {
            $scope.objCertificate.Destination.push({ Name: $scope.TextDestination, ClientID: $scope.ClientID });
            $scope.TextDestination = "";
            if ($scope.objCertificate.Destination.length >= 5) {
                $scope.objCertificate.DisableDestination = true;
            }
            //txtDestination.focus();
        }
    }

    // Elimina destinos de una lista
    $scope.removeDestination = function (item) {
        var index = $scope.objCertificate.Destination.indexOf(item);
        $scope.objCertificate.Destination.splice(index, 1);
        if ($scope.objCertificate.Destination.length < 10) {
            $scope.objCertificate.DisableDestination = false;
        }
    }

    // Agrega conductores a una lista
    $scope.addDriver = function () {
        if ($scope.searchTextDriver != "") {
            $scope.objCertificate.DriversList.push($scope.itemDriverCert);
            //se actualizan los datos del grid
            $scope.gridDriverCert.data = $scope.objCertificate.DriversList;
            $scope.searchTextDriver = "";
            if ($scope.objCertificate.DriversList.length >= 6) {

                $scope.objCertificate.DisableDriver = true;
            }
            //searchTextDriver.focus();
        }
    }

    // Elimina conductores de una lista
    $scope.removeDriver = function (item) {
        var index = $scope.objCertificate.DriversList.indexOf(item);
        $scope.objCertificate.DriversList.splice(index, 1);
        //se actualizan los datos del grid
        $scope.gridDriverCert.data = $scope.objCertificate.DriversList;
        if ($scope.objCertificate.DriversList.length < 6) {
            $scope.objCertificate.DisableDriver = false;
        }
    }

    // habilita el campo fecha final
    $scope.ChangeDates = function () {
        $scope.minEndDate = $scope.objCertificate.InitialDate;
        $scope.objCertificate.DisableEndDate = false;
        $scope.objCertificate.EndDate = null;
    }

    //Valida la licencia de conducción y existencia del conductor
    $scope.ValidateDriverCert = function () {
        if ($scope.itemDriverCert != null)
        {
            if ((typeof ($scope.itemDriverCert.LicenseValidity) !== "undefined") && ($scope.itemDriverCert.LicenseValidity != null) && ($scope.itemDriverCert.LicenseValidity != ""))
            {
                var LicenceDate = $scope.itemDriverCert.LicenseValidity;
                //damos formato a la fecha
                var format = formatDate(LicenceDate);
                LicenceDate = format;

                var fch = new Date(LicenceDate.getFullYear(), LicenceDate.getMonth(), LicenceDate.getDate());
                //buscamos si existe el item en la lista
                var index = $scope.objCertificate.DriversList.indexOf($scope.itemDriverCert);

                if (fch < $scope.minDate) {
                    $scope.showPopUpMessage("La licencia del conductor seleccionado ha caducado", "Por favor seleccione otro conductor o actualice la licencia de conducción");
                }
                else if (index >= 0) {
                    $scope.showPopUpMessage("Conductor asignado", "Este conductor ya fue seleccionado");
                }
                else {
                    $scope.addDriver();
                }
            }
        }
    }

    // Muestra un PopUp informativo con un mensaje, 
    // recive cómo parámetros el título y el cuerpo del mensaje
    $scope.showPopUpMessage = function (tittle, message) {
        var confirm = $mdDialog.confirm()
          .title(tittle)
          .textContent(message)
          .ariaLabel('Ventana de confirmación')
          .ok('OK');
        $mdDialog.show(confirm).then(function () {
            CleanDriver();
        }, function () { });
    }

    // Procesos del modal y de la generacion del archivo PDF
    // Funciones utilizadas para generar el PDF
    $scope.functionsPDF = {
        // Realiza la vista previa del archivo a PDF
        previewPDF: function () {
            var pdf = new jsPDF('p', 'pt', 'letter');

            html2pdf(document.getElementById("contentFUECCertificatePage1"), pdf, function (pdf) {
                pdf.addPage();
                html2pdf(document.getElementById("contentFUECCertificatePage2"), pdf, function (pdf) {
                    pdf.save("VISTA_PREVIA_" + randomCodPDF());
                });
            });
        },
        // Realiza la descarga del archivo a PDF
        savePDF: function (sendMailFlat, clientEmailAux) {
            $scope.gcOpenModalLoad();
            $scope.objCertificate.objConsecutive = "";
            $scope.objCertificate.objConsecutiveData = {};
            $scope.objCertificate.objConsecutiveData.ClientID = $scope.ClientID;//
            $scope.objCertificate.objConsecutiveData.ContractorID = $scope.objCertificate.objContractor.IDContractor;
            getConsecutive(sendMailFlat, clientEmailAux);
        },
    };

    // Realiza las validaciones de los campos del formulario.
    $scope.ValidateRegion = function () {
        $scope.objCertificate.validationResult = {};
        $scope.objCertificate.validationResult.Message = [];
        $scope.objCertificate.validationResult.Validate = false;

        if ($scope.objCertificate.objContractor != null &&
            $scope.certificateForm.NIT.$valid == true &&
            $scope.certificateForm.certificateContractObject.$valid == true &&
            $scope.certificateForm.certificateStateList.$valid == true &&
            $scope.certificateForm.certificateCityList.$valid == true &&
            $scope.objCertificate.objVehicleCert != null &&
            $scope.certificateForm.Model.$valid == true &&
            $scope.certificateForm.Trademark.$valid == true &&
            $scope.certificateForm.Type.$valid == true &&
            $scope.certificateForm.InternalNumber.$valid == true &&
            $scope.certificateForm.OperationCard.$valid == true &&
            $scope.objCertificate.DisableInitialDate == false &&
            $scope.certificateForm.InitialDate.$valid == true &&
            $scope.certificateForm.EndDate.$valid == true
            ) {
            $scope.objCertificate.validationResult.Validate = true;
        }
        else {
            $scope.objCertificate.validationResult.Validate = false;
        }

        return $scope.objCertificate.validationResult;
    }

    // Realiza las validaciones de los campos del formulario.
    $scope.ValidateRegionList = function () {
        var countErr = 0;
        $scope.objCertificate.validationResult = {};
        $scope.objCertificate.validationResult.Message = [];
        $scope.objCertificate.validationResult.Validate = true;

        if ($scope.objCertificate.DriversList.length <= 0) {
            $scope.objCertificate.validationResult.Message.push("Debe agregar al menos un conductor.");
            countErr++;
        }

        if ($scope.objCertificate.Destination.length <= 0) {
            $scope.objCertificate.validationResult.Message.push("Debe agregar al menos un destino.");
            countErr++;
        }

        if (countErr > 0) {
            $scope.objCertificate.validationResult.Validate = false;
        }

        return $scope.objCertificate.validationResult;
    }

    // Modal que contiene las funcionalidades permitidas para generar el PDF
    $scope.showModalCertificate = function (ev) {
        $mdDialog.show({
            locals: {
                functionsDialog: $scope
            },
            controller: function ($scope, $mdDialog, functionsDialog) {
                $scope.selected = false;
                $scope.objEmail = {};
                $scope.objEmail.clientEmail = "";
                $scope.ValidateRegion = functionsDialog.ValidateRegion().Validate;

                $scope.hide = function () {
                    $mdDialog.hide();
                };

                $scope.cancel = function () {
                    $mdDialog.cancel();
                };

                $scope.toggle = function (selected) {
                    $scope.selected = selected;
                    if (!$scope.selected) {
                        $scope.objEmail = {};
                        $scope.objEmail.clientEmail = null;
                        $scope.modalForm.$setPristine();
                        $scope.modalForm.$setUntouched();
                    }
                };

                $scope.previewDocument = function () {
                    functionsDialog.functionsPDF.previewPDF();
                }

                $scope.generatePDF = function () {
                    var continueProcess = false;

                    if ($scope.selected) {
                        if ($scope.modalForm.clientEmail.$valid) {
                            continueProcess = true;
                        }
                    }
                    else {
                        continueProcess = true;
                    }

                    if (continueProcess) {
                        if (functionsDialog.ValidateRegionList().Validate == true) {
                            var confirm = $mdDialog.confirm()
                              .title('Está a punto de generar el certificado FUEC')
                              .textContent('¿Desea generar el certificado FUEC?')
                              .ariaLabel('Ventana de confirmación')
                              .ok('OK')
                              .cancel('CANCELAR');
                            $mdDialog.show(confirm).then(function () {
                                if ($scope.selected) {
                                    $scope.sendMailFlat = true;
                                    $scope.clientEmailAux = $scope.objEmail.clientEmail;
                                } else {
                                    $scope.sendMailFlat = false;
                                    $scope.clientEmailAux = {}; 
                                }

                                functionsDialog.functionsPDF.savePDF( $scope.sendMailFlat,$scope.clientEmailAux);
                            }, function () { });
                        }
                        else {
                            functionsDialog.gcOpenAlert(functionsDialog.ValidateRegionList().Message);
                        }
                    }
                };
            },
            templateUrl: 'templates/certificate/modalCertificate.html',
            parent: angular.element(document.body),
            targetEvent: ev
        });
    };

    // <<<<<<<<<<<<<<<<<<<<<< Funciones que no necesariamente se deben registrar en el $scope >>>>>>>>>>>>>>>>>>>>>>

    // Inicialización de funciones
    function InitCertificate() {
        $scope.gcOpenModalLoad();
        getContractorsPlate($scope.ClientID);
        getContractObjects($scope.ClientID);
        getStates();
        getVehiclesAutoComp($scope.ClientID);
        getDriversCertificate($scope.ClientID);
        getCompanyCertificate();
        $scope.gcCloseModalLoad();
    }

    // Se crea una función para el filtro de tipo string
    function createFilterFor(searchText) {
        var lowercaseQuery = angular.lowercase(searchText);

        return function filterFn(item) {
            if ((item.value2.indexOf(lowercaseQuery) === 0)) {
                return (item.value2.indexOf(lowercaseQuery) === 0);
            }
            else {
                //filtra por defecto por la primera columna
                return (item.value.indexOf(lowercaseQuery) === 0);
            }

        };
    }

    // Validación de vigencia de revisión técnica
    function DateValidity() {
        var TechnicalReviewValidity = $scope.objCertificate.objVehicleCert.TechnicalReviewValidity;
        var lastDay = new Date(TechnicalReviewValidity.getFullYear(), TechnicalReviewValidity.getMonth() + 1, 0);
        var CurrentDayTechnical = TechnicalReviewValidity.getDate();
        var f1 = new Date(TechnicalReviewValidity.getFullYear(), TechnicalReviewValidity.getMonth(), TechnicalReviewValidity.getDate());

        if (f1 < $scope.minDate) {
            $scope.objCertificate.DisableInitialDate = true;
            $scope.objCertificate.DisableEndDate = true;
            $scope.ShowMessageVehicle = true;
        }
        else {
            $scope.ShowMessageVehicle = false;
            $scope.objCertificate.DisableInitialDate = false;
            if (CurrentDayTechnical >= 20) {
                $scope.maxDate = new Date(TechnicalReviewValidity.getFullYear(), TechnicalReviewValidity.getMonth(), lastDay.getDate());
                $scope.maxDateInitial = $scope.maxDate;//TechnicalReviewValidity;
            }
            else {
                $scope.maxDate = TechnicalReviewValidity;
                $scope.maxDateInitial = TechnicalReviewValidity;
            }
        }
    }

    // Obtiene la extension de la imagen que se encuentra en base64
    function guessImageMime(data) {
        if (data.charAt(0) == '/') {
            return "image/jpeg";
        } else if (data.charAt(0) == 'R') {
            return "image/gif";
        } else if (data.charAt(0) == 'i') {
            return "image/png";
        }
    }

    // Función para castear el formato de fechas del aplicativo
    function formatDate(originalDate) {
        if (originalDate == null || originalDate == "") {
            return null;
        }
        else {
            var s = originalDate + " 00:00";
            var bits = s.split(/\D/);
            var date = new Date(bits[2], --bits[1], bits[0], bits[3], bits[4]);

            return date;
        }
    }

    // Limpia el contratante
    function CleanContractor() {
        $scope.searchTextPlates = "";
        $scope.searchItemPlate = null;
        $scope.objCertificate.objContractor = null;
    }

    // Limpia el objeto vehículo
    function CleanVehicle() {
        $scope.searchTextVehicles = "";
        $scope.searchItemVehicle = null;
        $scope.objCertificate.objVehicleCert = null;
        $scope.objCertificate.InitialDate = null;
        $scope.objCertificate.EndDate = null;
        $scope.objCertificate.DisableInitialDate = true;
        $scope.objCertificate.DisableEndDate = true;
        $scope.ShowMessageVehicle = false;
        $scope.ShowMessageLicence = false;
    }

    // Limpia el objeto Driver
    function CleanDriver() {
        $scope.searchTextDriver = "";
        $scope.searchItemDriver = null;
        $scope.itemDriverCert = null;
    }

    // <<<<<<<<<<<<<<<<<<<<<< Funciones que acceden a los métodos expuestos en el Web API >>>>>>>>>>>>>>>>>>>>>>

     //Obtiene los contratantes por IDCliente desde el API
    function getContractorsPlate(ClientID) {
        certificateServiceCpt.getContractorsListCert(ClientID, $scope.Token)
        .then(function (data) {
            //Se crean dos variables para filtrar en minúscula sin modificar el objeto
            $scope.arrContractors = data.map(function (repo) {
                repo.value = repo.NIT.toLowerCase();
                repo.value2 = repo.CompanyName.toLowerCase();

                return repo;
            });
        })
        .catch(function (data, status) {
            console.error('Gists error', status, data);
        });
    }

    // Obtiene los vehículos por IDCliente
    function getVehiclesAutoComp(ClientID) {
        certificateServiceCpt.getVehiclesAutoCom(ClientID, $scope.Token)
        .then(function (data) {

            $scope.arrayVehicles = data;
        })
        .catch(function (data, status) {
            console.error('Gists error', status, data);
        });
    }

    //Obtiene los objetos de contrato desde el Web API
    function getContractObjects(ClientID) {
        certificateServiceCpt.getContractObjects(ClientID, $scope.Token)
        .then(function (data) {
            $scope.ContractObjects = data;
        })
         .catch(function (data, status) {
             console.error('Gists error', status, data);
         });
    }

    //Obtiene los departamentos desde el Web API
    function getStates() {
        certificateServiceCpt.getStates($scope.Token)
        .then(function (data) {
            $scope.StateList = data;
        })
         .catch(function (data, status) {
             console.error('Gists error', status, data);
         });
    }

    //Obtiene las ciudades según el departamento desde el Web API
    function getCities(StateID) {
        certificateServiceCpt.getCities(StateID, $scope.Token)
        .then(function (data) {
            $scope.CityList = data;
        })
         .catch(function (data, status) {
             console.error('Gists error', status, data);
         });
    }

    //obtiene los datos del vehículo según el item seleccionado en el autocompletar.
    function getVehicle(ClientID, PhysicalID) {
        certificateServiceCpt.getVehicle(ClientID, PhysicalID, $scope.Token)
        .then(function (data) {
            var format = formatDate(data.TechnicalReviewValidity);
            data.TechnicalReviewValidity = format;
            var modelValue = data.Model;
            data.Model = modelValue == 0 ? null : data.Model;
            $scope.objCertificate.objVehicleCert = data;
            DateValidity();
        })
        .catch(function (data, status) {
            console.error('Gists error', status, data);
        });
    }

    //Obtiene la lista de conductores
    function getDriversCertificate(ClientID) {
        certificateServiceCpt.getDriversCertificate(ClientID, $scope.Token)
        .then(function (data) {
            $scope.arrDrivers = data;
        })
        .catch(function (data, status) {
            console.error('Gists error', status, data);
        });
    }

    // Obtenemos información registrada de la empresa según el IDCliente
    function getCompanyCertificate() {
        certificateServiceCpt.getCompanyCertificate($scope.ClientID, $scope.Token)
       .then(function (company) {
           if (company.ClientID != 0) {
               $scope.objCertificate.objCompany = company;
               $scope.objCertificate.objCompany.FirmExt = guessImageMime(company.Firm);
               $scope.objCertificate.objCompany.LogoExt = guessImageMime(company.Logo);
               $scope.objCertificate.objCompany.InformationContact = $sce.trustAsHtml(company.InformationContact.replace(/\n/g, " <br />"));
           }
       })
       .catch(function (data, status) {
           console.error('Gists error', status, data);
       });
    }

    // enviar correo con el certificado FUEC
    function sendMail() {
        certificateServiceCpt.sendMail($scope.objCertificate.objMail, $scope.Token)
        .then(function (data) {
            if (data.Successfull) {
                $scope.gcOpenToastSuccess(data.Messages);
            }
            else {
                $scope.gcOpenAlert(data.Messages);
            }
        })
        .catch(function (data, status) {
            console.error('Gists error', status, data);
        });
    }

    // Obtenemos el consecutivo
    function getConsecutive(sendMailFlat, clientEmailAux) {
        certificateServiceCpt.getConsecutive($scope.objCertificate.objConsecutiveData, $scope.Token)
       .then(function (data) {
           SaveCertificate(data, sendMailFlat, clientEmailAux);
        })
       .catch(function (data, status) {
           console.error('Gists error', status, data);
        });
    }

    // Método para guardar el certificado
    function SaveCertificate(consecutive, sendMailFlat, clientEmailAux) {
        $scope.objCertificate.objConsecutive = consecutive;
        $scope.previewDocument = 0;
        var pdf = new jsPDF('p', 'pt', 'letter');
        
        html2pdf(document.getElementById("contentFUECCertificatePage1"), pdf, function (pdf) {
            pdf.addPage();
            html2pdf(document.getElementById("contentFUECCertificatePage2"), pdf, function (pdf) {
                pdf = new jsPDF('p', 'pt', 'letter');

                html2pdf(document.getElementById("contentFUECCertificatePage1"), pdf, function (pdf) {
                    pdf.addPage();
                    html2pdf(document.getElementById("contentFUECCertificatePage2"), pdf, function (pdf) {
                        $scope.objCertificate.objConsecutive = consecutive;
                        $scope.objCertificate.objCertificateDTOSave = {};
                        $scope.objCertificate.objCertificateDTOSave.FUECCode = consecutive;
                        $scope.objCertificate.objCertificateDTOSave.Contract = $scope.objCertificate.objContractor.Contract;
                        $scope.objCertificate.objCertificateDTOSave.NITContractor = $scope.objCertificate.objContractor.NIT;
                        $scope.objCertificate.objCertificateDTOSave.CompanyNameContractor = $scope.objCertificate.objContractor.CompanyName;
                        $scope.objCertificate.objCertificateDTOSave.ObjectContractor = $scope.objCertificate.ContractObjectValue;
                        $scope.objCertificate.objCertificateDTOSave.Plate = $scope.objCertificate.objVehicleCert.Plate;
                        $scope.objCertificate.objCertificateDTOSave.StartDate = $filter('date')($scope.objCertificate.InitialDate, 'dd/MM/yyyy');
                        $scope.objCertificate.objCertificateDTOSave.FinishDate = $filter('date')($scope.objCertificate.EndDate, 'dd/MM/yyyy');
                        $scope.objCertificate.objCertificateDTOSave.ClientID = $scope.ClientID;
                        $scope.objCertificate.objCertificateDTOSave.PDFFUEC = pdf.output('datauristring').replace("data:application/pdf;base64,", "");
                        $scope.objCertificate.objCertificateDTOSave.ExpeditionDate = $filter('date')(new Date(), 'dd/MM/yyyy');

                        //registro contadores de uso
                        $scope.userInfo = {};
                        $scope.userInfo.ClientID = $scope.ClientID;
                        $scope.userInfo.UserName = $scope.UserPage;
                        $scope.userInfo.TypeAccess = "Generación certificado FUEC";
                        certificateServiceCpt.UseRegister($scope.userInfo, $scope.Token);

                        certificateServiceCpt.SaveCertificate($scope.objCertificate.objCertificateDTOSave, $scope.Token)
                        .then(function (Data) {
                            if (Data.Successfull) {
                                pdf = new jsPDF('p', 'pt', 'letter');

                                html2pdf(document.getElementById("contentFUECCertificatePage1"), pdf, function (pdf) {
                                    pdf.addPage();
                                    html2pdf(document.getElementById("contentFUECCertificatePage2"), pdf, function (pdf) {
                                        pdf.save("CERTIFICADO_FUEC_" + randomCodPDF());

                                        if (sendMailFlat) {
                                            $scope.objFile64 = {};
                                            $scope.objFile64.fileByte = pdf.output('datauristring').replace("data:application/pdf;base64,", "");
                                            $scope.objFile64.nameFile = 'CertificadoFUEC_' + $scope.objCertificate.objCertificateDTOSave.Plate + '_' + dateGenerate + '.pdf';
                                            $scope.objCertificate.objMail.recipient = clientEmailAux;
                                            $scope.objCertificate.objMail.subject = "Certificado FUEC Generado " + dateGenerate;
                                            $scope.objCertificate.objMail.body = "Certificado FUEC_" + dateGenerate;
                                            $scope.objCertificate.objMail.nameAndFilebase64 = [$scope.objFile64];

                                            sendMail();
                                        }

                                        CertificateClean();
                                        $scope.showPopUpMessage("Operación exitosa", "FUEC generado correctamente");
                                        $scope.gcCloseModalLoad();
                                    });
                                });
                            }
                            else {
                                $scope.showPopUpMessage("Operación fallida", "Ocurrió un error inesperado en la generación del FUEC");
                                pdf = new jsPDF('p', 'pt', 'letter');
                            }
                        })
                        .catch(function (data, status) {
                            $scope.showPopUpMessage("Operación fallida", "Ocurrió un error inesperado en la generación del FUEC");
                            console.error('Gists error', status, data);
                        });
                    });
                });            
            });
        });
    }

    // Limpia el formulario para dejarlo por defecto
    function CertificateClean() {
        //Limpia contratante y nit contratante
        CleanContractor();
        //Limpia el objeto vehículo
        CleanVehicle();
        //Limpia el objeto conductor
        CleanDriver();
        //Reestablece el campo objeto de contrato
        $scope.objCertificate.ContractObjectValue = null;
        //Reestablece el campo departamento origen.
        $scope.objCertificate.StateValue = null;
        //Reestablece el campo ciudad origen.
        $scope.objCertificate.CityValue = null;
        //Etablece la propiedad disable del campo ciudad origen en true
        $scope.objCertificate.DisableCityList = true;
        //Limpia la lista de destinos.
        $scope.objCertificate.Destination = [];
        //Habilita el campo destino
        $scope.objCertificate.DisableDestination = false;
        //Limpia el textbox destino
        $scope.TextDestination = "";
        //Limpia la tabla de conductores y la lista de conductores
        $scope.objCertificate.DriversList = [];
        $scope.gridDriverCert.data = $scope.objCertificate.DriversList;
        //Habilita el campo conductor
        $scope.objCertificate.DisableDriver = false;
        //Limpia el campo conductor
        $scope.searchTextDriver = "";
        $scope.certificateForm.$setUntouched();
        $scope.certificateForm.$setPristine();
        //Se setea variable para indicar que el documento sera previsualizado
        $scope.previewDocument = 1;
    }

    function randomCodPDF() {
        var date = new Date();
        var dd = date.getDate();
        var mm = date.getMonth() + 1;
        var yyyy = date.getFullYear();

        if (dd < 10) {
            dd = '0' + dd
        }

        if (mm < 10) {
            mm = '0' + mm
        }

        return yyyy + '' + mm + '' + dd + '_' + Math.floor((Math.random() * 1000) + 1);
    }
}]);

