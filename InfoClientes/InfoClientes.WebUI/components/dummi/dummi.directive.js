﻿////Se usa este comando para validar que la sintaxis se esta haciendo de la forma correcta
//'use strict';

//// Directiva que obtiene la primera página del certificado
//app.directive("certificatePage1", function () {
//    return {
//        restrict: "E",
//        templateUrl: "../../templates/certificate/page1.html",
//        scope: true, // creates its own local scope
//        replace: true,
//        link: function (scope, element, attributes) {
//            scope.$watch('objCertificate', function (data) {
//                scope.objCertificate = {};
//                // works almost fine but in 2nd case data is also filled
//                scope.objCertificate = angular.fromJson(data);
//            })
//        }
//    }
//});

//// Directiva que obtiene la primera página del certificado
//app.directive('certificatePage2', function () {
//    return {
//        templateUrl: '../../templates/certificate/page2.html'
//    };
//});