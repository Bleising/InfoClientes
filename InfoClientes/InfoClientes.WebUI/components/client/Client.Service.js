﻿// Se usa este comando para validar que la sintaxis se esta haciendo de la forma correcta
'use strict';


// Servicio del componente empresa
app.factory('clientServiceCpt', function ($http, $q, $httpParamSerializerJQLike, urlApi, $filter) {
    return {

        getClients: function () {
            var deferred = $q.defer();
            $http({
                method: 'GET',
                dataType: "JSON",
                url: urlApi.client.metGetclient,
                headers: {
                    'Content-Type': 'application/json'
                },
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                // when the response is available
                deferred.resolve(response.data);
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                deferred.resolve()
            });
            return deferred.promise;
        },

        getCities: function (DepartamentID) {
            var deferred = $q.defer();
            $http({
                method: 'GET',
                dataType: "JSON",
                url: urlApi.masterLocation.metGetCities + DepartamentID,
                headers: {
                    'Content-Type': 'application/json'
                },
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                // when the response is available
                deferred.resolve(response.data);
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                deferred.resolve()
            });
            return deferred.promise;
        },

        getDepartaments: function (CountryID) {
            var deferred = $q.defer();
            $http({
                method: 'GET',
                dataType: "JSON",
                url: urlApi.masterLocation.metGetDepartaments + CountryID,
                headers: {
                    'Content-Type': 'application/json'
                },
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                // when the response is available
                deferred.resolve(response.data);
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                deferred.resolve()
            });
            return deferred.promise;
        },

        getCountrie: function () {
            var deferred = $q.defer();
            $http({
                method: 'GET',
                dataType: "JSON",
                url: urlApi.masterLocation.metGetCountries,
                headers: {
                    'Content-Type': 'application/json'
                },
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                // when the response is available
                deferred.resolve(response.data);
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                deferred.resolve()
            });
            return deferred.promise;
        },
        registerClient: function (objClient) {
            //var formatDate = $filter('date')(objDriver.LicenseValidity, 'dd/MM/yyyy');
            //objDriver.LicenseValidity = formatDate;
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: urlApi.client.metPostSaveOrUpdateClient,
                dataType: "json",
                contentType: "application/json charset=utf-8",
                data: JSON.stringify(objClient),
                headers: {
                    'Content-Type': 'application/json'
                },
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                deferred.resolve(response.data);
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                deferred.resolve()
            });
            return deferred.promise;
        },
        //updateDriver: function (objDriver, token) {
        //    var formatDate = $filter('date')(objDriver.LicenseValidity, 'dd/MM/yyyy');
        //    objDriver.LicenseValidity = formatDate;
        //    var deferred = $q.defer();
        //    $http({
        //        method: 'POST',
        //        url: urlApi.driver.metPutDriver,
        //        dataType: "json",
        //        contentType: "application/json charset=utf-8",
        //        data: JSON.stringify(objDriver),
        //        headers: {
        //            'Content-Type': 'application/json',
        //            'Authorization': token
        //        },
        //    }).then(function successCallback(response) {
        //        // this callback will be called asynchronously
        //        deferred.resolve(response.data);
        //    }, function errorCallback(response) {
        //        // called asynchronously if an error occurs
        //        // or server returns response with an error status.
        //        deferred.resolve()
        //    });
        //    return deferred.promise;
        //},
        //deleteDriver: function (DriverID, token) {
        //    var deferred = $q.defer();
        //    $http({
        //        method: 'POST',
        //        dataType: "json",
        //        contentType: 'application/json',
        //        url: urlApi.driver.metDeleteDriver + DriverID,
        //        headers: {
        //            'Content-Type': 'application/json',
        //            'Authorization': token
        //        },
        //    }).then(function successCallback(response) {
        //        // this callback will be called asynchronously
        //        deferred.resolve(response.data);
        //    }, function errorCallback(response) {
        //        // called asynchronously if an error occurs
        //        // or server returns response with an error status.
        //        deferred.resolve()
        //    });

        //    return deferred.promise;
        //},
    };
})
