﻿//Se usa este comando para validar que la sintaxis se esta haciendo de la forma correcta
'use strict';

app.controller("clientControllerCpt", ['$scope', 'clientServiceCpt', '$mdDialog', '$filter', function ($scope, clientServiceCpt, $mdDialog, $filter) {

    // <<< Inicializacion de funciones y metodos >>>
    Init();

    // <<< Funciones registradas en el $scope >>> 
    // Esta funcion valida los campos indicados para el registro y la actualizacion del conductor
    // Retorna true siempre y cuando todo lo diligenciado sea correcto
    $scope.validateRegion1 = function () {
        if ($scope.clientForm.Name.$valid == true &&
            $scope.clientForm.NIT.$valid == true &&
            $scope.clientForm.Address.$valid == true &&
            $scope.clientForm.Phone.$valid == true &&
            $scope.clientForm.Quota.$valid == true &&
            $scope.clientForm.PercentageVisit.$valid == true &&
            $scope.clientForm.cityList.$valid == true &&
            $scope.clientForm.departamentList.$valid == true &&
            $scope.clientForm.countryList.$valid == true) {
            return false;
        }
        else {
            return true;
        }
    }

    $scope.createClient = function () {
        if (!$scope.validateRegion1()) {
            if ($scope.isNew) {
                saveClient();
            }
            else {
                updateClient();
            }
        }
    }

    $scope.editClient = function (item) {

        $scope.objClient.ClientID = item.ClientID;
        $scope.objClient.Name = item.Name;
        $scope.objClient.NIT = item.NIT;
        $scope.objClient.Address = item.Address;
        $scope.objClient.Phone = item.Phone;
        $scope.objClient.Quota = item.Quota;
        $scope.objClient.BalanceQuota = item.BalanceQuota;
        $scope.objClient.PercentageVisit = item.PercentageVisit;
        RefreshList(item.City.Department.Country.CountryID, item.City.Department.DepartmentID);
        $scope.objClient.Country = item.City.Department.Country.CountryID;
        $scope.objClient.Departament = item.City.Department.DepartmentID;
        $scope.objClient.City = item.City.CityID;

        console.log($scope.objClient.City);
        $scope.isNew = false;
        window.scrollTo(0, 0);
    }

    // <<< Procesos para validar el formulario del módulo cliente >>>
    // Procesos del DataTable adaptado con angular js y angula rmaterial
    $scope.toggle = function (item, list) {
        var idx = list.indexOf(item);
        if (idx > -1) {
            list.splice(idx, 1);
        }
        else {
            list.push(item);
        }
    };

    function RefreshList(CountryID, DepartamentID)
    {
        $scope.objClient.Country = CountryID;
        getDepartaments(CountryID);
        getCities(DepartamentID);
    }

    ////Elimina un conductor
    //$scope.deleteDriver = function (item) {
    //    var confirm = $mdDialog.confirm()
    //      .title('Está a punto de eliminar un conductor')
    //      .textContent('¿Desea eliminar el conductor seleccionado?')
    //      .ariaLabel('Ventana de confirmación')
    //      .ok('OK')
    //      .cancel('CANCELAR');
    //    $mdDialog.show(confirm).then(function () {
    //        deleteDriver(item);
    //    }, function () { });
    //}

    // <<< Funciones que no necesariamente se deben registrar en el $scope >>>
    // Inicialización de funciones
    function Init() {
        var today = new Date();
        $scope.minDate = new Date(today.getFullYear(), today.getMonth(), today.getDate());
        $scope.objClient = { ClientObject: [] };
        $scope.gridClient = { data: [] };
        $scope.ValidarAlfanumerico = new RegExp("^[wáéíóúÁÉÍÓÚ\\w\\s\\.\\ñÑ]+$", "i");
        $scope.ValidarSoloNumero = new RegExp("^([0-9])*$");
        $scope.citiesList = [];
        $scope.departamentsList = [];
        $scope.countriesList = [];
        $scope.disableDepartament = true;
        $scope.disableCities = true;
        $scope.isNew = true;
        $scope.searchInput = "";
        $scope.gcOpenModalLoad();
        getClients();
        getCountries();
        //$scope.objClient.City = 1;
    }

    $scope.getCitiesList = function () {
        getCities($scope.objClient.Departament);
    }

    $scope.getDepartamentList = function () {
        getDepartaments($scope.objClient.Country);
    }

    function getClients() {
        clientServiceCpt.getClients()
        .then(function (data) {
            $scope.gcCloseModalLoad();
            $scope.gridClient.data = data;
        })
        .catch(function (data, status) {
            $scope.gcCloseModalLoad();
            console.error('Gists error', status, data);
        });
    }

    // Obtenemos de la web Api la lista de ciudades
    function getCities(DepartamentID) {
        clientServiceCpt.getCities(DepartamentID)
        .then(function (data) {
            $scope.citiesList = data;
            $scope.disableCities = false;
        })
         .catch(function (data, status) {
             console.error('Gists error', status, data);
         });
    }


    function getDepartaments(CountryID) {
        clientServiceCpt.getDepartaments(CountryID)
        .then(function (data) {
            $scope.departamentsList = data;
            $scope.disableDepartament = false;
        })
         .catch(function (data, status) {
             console.error('Gists error', status, data);
         });
    }

    function getCountries() {
        clientServiceCpt.getCountrie()
        .then(function (data) {
            $scope.countriesList = data;
        })
         .catch(function (data, status) {
             console.error('Gists error', status, data);
         });
    }
    //// Registra el conductor 
    //function saveDriver() {
    //    $scope.gcOpenModalLoad();
    //    var objDriver = setobjDriver($scope.objDriver);
    //    driverServiceCpt.registerDriver(objDriver, $scope.Token)
    //    .then(function (data) {
    //        if (data.Successfull) {
    //            $scope.gcOpenToastSuccess([data.Messages[0]]);
    //            CleanForm();
    //        }
    //        else {
    //            $scope.gcOpenAlert(data.Messages);
    //        }

    //        //Actualizamos la Grid de vehículos
    //        getDrivers($scope.ClientID);
    //    })
    //    .catch(function (data, status) {
    //        $scope.gcCloseModalLoad();
    //        console.error('Gists error', status, data);
    //    });
    //}

    //Actualización de conductor
    function updateClient() {
        $scope.gcOpenModalLoad();
        var objClient = setobjDriver($scope.objClient);
        clientServiceCpt.registerClient(objClient)
        .then(function (data) {
            if (data.Successfull) {
                //$scope.DisableConsecutive = true;
                $scope.gcOpenToastSuccess(data.Messages);
                $scope.isNew = true;
                getClients();
                CleanForm();
            }
            else {
                $scope.gcCloseModalLoad();
                $scope.gcOpenAlert(data.Messages);
            }
        })
        .catch(function (data, status) {
            console.error('Gists error', status, data);
            $scope.isNew = true;
            CleanForm();
            $scope.gcCloseModalLoad();
        });
    }

    //Guarda un cliente
    function saveClient() {
        $scope.gcOpenModalLoad();
        var objClient = setobjDriver($scope.objClient);
        objClient.ClientID = 0;
        clientServiceCpt.registerClient(objClient)
        .then(function (data) {
            if (data.Successfull) {
                //$scope.DisableConsecutive = true;
                $scope.gcOpenToastSuccess(data.Messages);
                $scope.isNew = true;
                getClients();
                CleanForm();
            }
            else {
                $scope.gcCloseModalLoad();
                $scope.gcOpenAlert(data.Messages);
            }
        })
        .catch(function (data, status) {
            console.error('Gists error', status, data);
            $scope.isNew = true;
            CleanForm();
            $scope.gcCloseModalLoad();
        });
    }
    ////Eliminar conductor
    //function deleteDriver(driver) {
    //    $scope.gcOpenModalLoad();
    //    driverServiceCpt.deleteDriver(driver.DriverID, $scope.Token)
    //    .then(function (data) {
    //        if (data.Successfull) {
    //            $scope.DisableConsecutive = true;
    //            $scope.gcOpenToastSuccess(data.Messages);
    //            $scope.isNew = true;
    //            getDrivers($scope.ClientID);
    //            CleanForm();
    //        }
    //        else {
    //            $scope.gcCloseModalLoad();
    //            $scope.gcOpenAlert(data.Messages);
    //        }
    //    })
    //    .catch(function (data, status) {
    //        $scope.gcCloseModalLoad();
    //        console.error('Gists error', status, data);
    //        $scope.isNew = true;
    //        CleanForm();
    //    });
    //}

    // Limpia el formulario
    function CleanForm() {
        $scope.searchTextDrivers = "";
        $scope.searchItemDriver = null;
        $scope.objClient = {};
        $scope.ClientForm.$setPristine();
        $scope.ClientForm.$setUntouched();
        $scope.isNew = true;
    }

    function setobjDriver(objClient) {

        var objClientNew = { City:[] };
        objClientNew.ClientID = objClient.ClientID;
        objClientNew.Name = objClient.Name;
        objClientNew.NIT = objClient.NIT;
        objClientNew.Phone = objClient.Phone;;
        objClientNew.Quota = objClient.Quota;
        objClientNew.BalanceQuota = objClient.BalanceQuota;
        objClientNew.PercentageVisit = objClient.PercentageVisit;
        objClientNew.City.CityID = $scope.objClient.City;
        objClientNew.Address = objClient.Address;

        return objClientNew;
    }



}]);

