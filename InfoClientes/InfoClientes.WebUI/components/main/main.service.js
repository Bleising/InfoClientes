﻿// Se usa este comando para validar que la sintaxis se esta haciendo de la forma correcta
'use strict';

// Servicio del componente contratante
app.factory('mainServiceCpt', function ($http, $q, $httpParamSerializerJQLike, urlApi, $filter) {
    return {
        getContractorsListCert: function (ClientID, token) {

            var deferred = $q.defer();
            $http({
                method: 'GET',
                dataType: "JSON",
                url: urlApi.Contractor.metGetContractors + ClientID,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': token
                },
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                // when the response is available

                //response.data = mockTest;
                deferred.resolve(response.data);

            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                deferred.resolve()
            });

            //deferred.resolve(mockData);
            return deferred.promise;
        }
    }
});