﻿// Se usa este comando para validar que la sintaxis se esta haciendo de la forma correcta
'use strict';

// Servicio del componente empresa
app.factory('companyServiceCpt', function ($http, $q, $httpParamSerializerJQLike, urlApi) {
    return {
        getTerritorialCode: function (token) {
            var deferred = $q.defer();
            $http({
                method: 'GET',
                dataType: "JSON",
                url: urlApi.company.metGetTerritorialCode,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': token
                },
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                // when the response is available
                deferred.resolve(response.data);
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                deferred.resolve()
            });
            return deferred.promise;
        },
        getCompany: function (ClientID, token) {
            var deferred = $q.defer();
            $http({
                method: 'GET',
                dataType: "JSON",
                url: urlApi.company.metGetCompany + ClientID,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': token
                },
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                // when the response is available
                deferred.resolve(response.data);
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                deferred.resolve()
            });
            return deferred.promise;
        },
        registerCompany: function (objCompany, token) {
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: urlApi.company.metPostSaveCompany,
                dataType: "json",
                contentType: "application/json charset=utf-8",
                data: JSON.stringify(objCompany),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': token
                },
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                deferred.resolve(response.data);
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                deferred.resolve()
            });
            return deferred.promise;
        },
        updateCompany: function (objCompany, token) {
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: urlApi.company.metPutCompany,
                dataType: "json",
                contentType: "application/json charset=utf-8",
                data: JSON.stringify(objCompany),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': token
                },
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                deferred.resolve(response.data);
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                deferred.resolve()
            });
            return deferred.promise;
        },
    };
})
