﻿//Se usa este comando para validar que la sintaxis se esta haciendo de la forma correcta
'use strict';

// Transformamos función readAsBinaryString ya que no sirve en IE
if (!FileReader.prototype.readAsBinaryString) {
    FileReader.prototype.readAsBinaryString = function (fileData) {
        var binary = "";
        var pt = this;
        var reader = new FileReader();
        reader.onload = function (e) {
            var bytes = new Uint8Array(reader.result);
            var length = bytes.byteLength;
            for (var i = 0; i < length; i++) {
                binary += String.fromCharCode(bytes[i]);
            }
            pt.content = binary;
            pt.onload();
        }
        reader.readAsArrayBuffer(fileData);
    }
}

// Directivas company
// Inicio directivas input file *_*_*_*_*_*_*
app.directive('chooseFileCompanyLogo', function () {
    return {
        link: function (scope, elem, attrs) {
            var button = angular.element(elem[0].querySelector('button#uploadButtonLogo'));
            var input = angular.element(elem[0].querySelector('input#companyLogo'));
            button.bind('click', function () {
                input[0].click();
            });
            input.bind('change', function (e) {
                scope.$apply(function () {
                    scope.gcOpenModalLoad();

                    var files = e.target.files;
                    var file = files[0];
                    if (files[0]) {
                        if (validateExt(files[0])) {
                            var reader = new FileReader();
                            reader.onload = function (e) {
                                if (reader.result) reader.content = reader.result;
                                var myImage = new Image;
                                myImage.src = 'data:image/' + files[0].type.replace("image/", "") + ';base64,' + btoa(reader.content);
                                myImage.onload = function () {
                                    if ((parseInt(myImage.width) <= 300) && (parseInt(myImage.height) <= 150)) {
                                        scope.fileNameArchiveCompanyLogo = "(Imagen cargada)";
                                        scope.objCompany.Logo = btoa(reader.content);
                                        scope.gcCloseModalLoad();
                                    }
                                    else {
                                        scope.gcCloseModalLoad();
                                        scope.gcOpenAlert(["El tamaño de la imagen debe ser máximo de 300 X 150 píxeles."]);
                                    }
                                }
                            }
                            reader.readAsBinaryString(file);
                        }
                        else {
                            scope.gcCloseModalLoad();
                            scope.gcOpenAlert(["Únicamente se admiten imágenes con formato PNG, JPG o JPEG."]);
                        }
                    }
                    else {
                        scope.fileNameArchiveCompanyLogo = null;
                        scope.objCompany.Logo = "";
                        scope.gcCloseModalLoad();
                    }
                });
            });
        }
    };
});

app.directive('chooseFileCompanySignature', function () {
    return {
        link: function (scope, elem, attrs) {
            var button = angular.element(elem[0].querySelector('button#uploadButtonFirm'));
            var input = angular.element(elem[0].querySelector('input#companySignature'));
            button.bind('click', function () {
                input[0].click();
            });
            input.bind('change', function (e) {
                scope.$apply(function () {
                    scope.gcOpenModalLoad();

                    var files = e.target.files;
                    var file = files[0];
                    if (files[0]) {
                        if (validateExt(files[0])) {
                            var reader = new FileReader();
                            reader.onload = function (e) {
                                if (reader.result) reader.content = reader.result;
                                var myImage = new Image;
                                myImage.src = 'data:image/' + files[0].type.replace("image/", "") + ';base64,' + btoa(reader.content);
                                myImage.onload = function () {
                                    if ((parseInt(myImage.width) <= 300) && (parseInt(myImage.height) <= 150)) {
                                        scope.fileNameArchiveCompanySignature = "(Imagen cargada)";
                                        scope.objCompany.Firm = btoa(reader.content);
                                        scope.gcCloseModalLoad();
                                    }
                                    else {
                                        scope.gcCloseModalLoad();
                                        scope.gcOpenAlert(["El tamaño de la imagen debe ser máximo de 300 X 150 píxeles."]);
                                    }
                                }
                            }
                            reader.readAsBinaryString(file);
                        }
                        else {
                            scope.gcCloseModalLoad();
                            scope.gcOpenAlert(["Únicamente se admiten imágenes con formato PNG, JPG o JPEG."]);
                        }
                    }
                    else {
                        scope.fileNameArchiveCompanySignature = null;
                        scope.objCompany.Firm = "";
                        scope.gcCloseModalLoad();
                    }
                });
            });
        }
    };
});
// Fin directivas input file *_*_*_*_*_*_*

function validateExt(files) {
    var extension = files.type.replace("image/", "");
    if (extension == "png" || extension == "PNG" || extension == "jpg" || extension == "JPG" || extension == "jpeg" || extension == "JPEG") {
        return true;
    }
    else {
        return false;
    }
}