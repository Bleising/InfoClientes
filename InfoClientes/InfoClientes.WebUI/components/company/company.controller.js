﻿//Se usa este comando para validar que la sintaxis se esta haciendo de la forma correcta
'use strict';

// Controlador del componente empresa
app.controller("companyControllerCpt", ['$scope', 'companyServiceCpt', '$mdDialog', function ($scope, companyServiceCpt, $mdDialog) {
    // Inicialización de variables
    
    $scope.objCompany = { ContractObject: [] };
    $scope.FunctionalityButton = "";
    $scope.TextContractObject = "";
    $scope.TerritorialCodeObjects = [];
    $scope.ValidarAlfanumericoHyphen = new RegExp("^[-_\\w\\.\\ñÑ]+$", "i");
    $scope.ValidarAlfanumerico = new RegExp("^[wáéíóúÁÉÍÓÚ\\w\\s\\.\\ñÑ]+$", "i");
    $scope.ValidarSoloNumero = new RegExp("^([0-9])*$");
    $scope.DisableConsecutive = false;
    $scope.ValidarAlfanumericoComma = new RegExp("^[s\\wáéíóúÁÉÍÓÚ\\w+\\s\\,\\ñÑ]+$", "i");
    $scope.HabilitationYears = [{ "yy": "90", "year": 1990 }, { "yy": "91", "year": 1991 }
                              , { "yy": "92", "year": 1992 }, { "yy": "93", "year": 1993 }
                              , { "yy": "94", "year": 1994 }, { "yy": "95", "year": 1995 }
                              , { "yy": "96", "year": 1996 }, { "yy": "97", "year": 1997 }
                              , { "yy": "98", "year": 1998 }, { "yy": "99", "year": 1999 }
                              , { "yy": "00", "year": 2000 }, { "yy": "01", "year": 2001 }
                              , { "yy": "02", "year": 2002 }, { "yy": "03", "year": 2003 }
                              , { "yy": "04", "year": 2004 }, { "yy": "05", "year": 2005 }
                              , { "yy": "06", "year": 2006 }, { "yy": "07", "year": 2007 }
                              , { "yy": "08", "year": 2008 }, { "yy": "09", "year": 2009 }
                              , { "yy": "10", "year": 2010 }, { "yy": "11", "year": 2011 }
                              , { "yy": "12", "year": 2012 }, { "yy": "13", "year": 2013 }
                              , { "yy": "14", "year": 2014 }, { "yy": "15", "year": 2015 }
                              , { "yy": "16", "year": 2016 }, { "yy": "17", "year": 2017 }
                              , { "yy": "18", "year": 2018 }, { "yy": "19", "year": 2019 }
                              , { "yy": "20", "year": 2020 }
                              ];

    // Se inicializa lo necesario del aplicativo
    Init();
    
    // Funcionalidades del formulario, ejemplo registrar, actualizar, etc...
    // ---- ---- ----
    // Agrega objetos de contrato a una lista
    $scope.addContractObject = function () {
        if ($scope.TextContractObject != "") {
            $scope.objCompany.ContractObject.push({ Name: $scope.TextContractObject, ClientID: $scope.ClientID });
            $scope.TextContractObject = "";
            ContractObject.focus();
        }
    }

    // Elimina objetos de contrato a una lista
    $scope.removeContractObject = function (item) {
        var index = $scope.objCompany.ContractObject.indexOf(item);
        $scope.objCompany.ContractObject.splice(index, 1);
    }

    // Ejecuta funcionalidades del formulario de forma dinamica
    $scope.executeButton = function () {
        if ($scope.companyForm.$valid) {
            if ($scope.FunctionalityButton == "companyRegister") {
                if ($scope.isNew) {
                    registerCompany();
                }
                else {
                    updateCompany();
                }
            }
        }
    }

    // Agrega "0" para autocompletar un valor dependiendo el numero de caracteres
    $scope.zeroFill = function (obj) {
        var addZero = obj;
        var foo = '' + addZero;

        if (!isNaN(parseFloat(addZero)) && isFinite(addZero)) {
            switch (foo.length) {
                case 1:
                    foo = "000" + addZero;
                    break;
                case 2:
                    foo = '00' + addZero;
                    break;
                case 3:
                    foo = '0' + addZero;
                    break;
                default:
                    break;
            }
            obj = foo;
        }

        return obj;
    }

    // Permite ver la imagen adjunta en el campo
    $scope.viewImage = function (imgBase64) {
        if (imgBase64 !== undefined && imgBase64 !== null && imgBase64 !== "") {
            $scope.gcOpenAlertImage([imgBase64]);
        }
        else {
            $scope.gcOpenAlert(['No hay ninguna imagen adjunta.']);
        }
    }

    // Inicialización de funciones
    function Init() {
        getTerritorialCode();
        getCompany();
    }

    // registramos la información de la empresa
    function registerCompany() {
        $scope.gcOpenModalLoad();

        $scope.objCompany.ClientID = $scope.ClientID;
        companyServiceCpt.registerCompany($scope.objCompany, $scope.Token)
        .then(function (data) {
            if (data.Successfull) {
                $scope.DisableConsecutive = true;
                $scope.gcCloseModalLoad();
                $scope.gcOpenToastSuccess(data.Messages);
            }
            else {
                $scope.gcCloseModalLoad();
                $scope.gcOpenAlert(data.Messages);
            }
        })
        .catch(function (data, status) {
            $scope.gcCloseModalLoad();
            console.error('Gists error', status, data);
        });
    }

    // Actualizamos la información de la empresa
    function updateCompany() {
        $scope.gcOpenModalLoad();

        companyServiceCpt.updateCompany($scope.objCompany, $scope.Token)
        .then(function (data) {
            if (data.Successfull) {
                $scope.gcCloseModalLoad();
                $scope.gcOpenToastSuccess(data.Messages);
            }
            else {
                $scope.gcCloseModalLoad();
                $scope.gcOpenAlert(data.Messages);
            }
        })
        .catch(function (data, status) {
            $scope.gcCloseModalLoad();
            console.error('Gists error', status, data);
        });
    }

    // Obtenemos de la web Api los códigos territoriales
    function getTerritorialCode() {
        companyServiceCpt.getTerritorialCode($scope.Token)
        .then(function (data) {
            $scope.TerritorialCodeObjects = data;
        })
         .catch(function (data, status) {
             console.error('Gists error', status, data);
         });
    }

    // Obtenemos información registrada de la empresa según el IDCliente
    function getCompany() {
        $scope.gcOpenModalLoad();

        companyServiceCpt.getCompany($scope.ClientID, $scope.Token)
       .then(function (company) {
           $scope.isNew = true;
           if (company.ClientID != 0) {
               $scope.objCompany = company;
               $scope.isNew = false;
               $scope.DisableConsecutive = true;

               // Valores por default de acuerdo a las reglas de negocio 
               $scope.fileNameArchiveCompanyLogo = "(Imagen cargada)";
               $scope.fileNameArchiveCompanySignature = "(Imagen cargada)";
               $scope.objCompany.Consecutive = $scope.zeroFill($scope.objCompany.Consecutive);
           }

           $scope.gcCloseModalLoad();
       })
       .catch(function (data, status) {
           $scope.gcCloseModalLoad();
           console.error('Gists error', status, data);
       });
    }
}]);

// Limita saltos de linea de un campo textArea
function validateBRTextArea(e, obj) {
    var key = (document.all) ? e.keyCode : e.which;
    if (key != 13) return;
    var rows = 5
    var txt = obj.value.split('\n');
    return (txt.length < rows);
}
