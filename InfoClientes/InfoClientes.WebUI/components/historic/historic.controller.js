﻿//Se usa este comando para validar que la sintaxis se esta haciendo de la forma correcta
'use strict';

// Controlador del componente Certificado
app.controller("historicControllerCpt", ['$scope', 'historicServiceCpt', '$mdDialog', '$q', '$timeout', '$filter', function ($scope, historicServiceCpt, $mdDialog, $q, $timeout, $filter) {
    var today = new Date();
    var dateGenerate = today.getFullYear() + '_' + (today.getMonth() + 1) + '_' + today.getDate();

    $scope.gridHistoric = { data: [] };
    $scope.objFUEC = {};
    $scope.getPDFFUEC = "";

    // <<< Inicializacion de funciones y metodos >>>
    getContractor($scope.ClientID);

    $scope.refreshGrid = function ()
    {
       $scope.gridHistoric.grid.paginationOptions.currentPage = 1;      
    };

    $scope.DownloadPDF = function (item) {
        getPDFBase(item);
    }

    function dataURLtoFile(dataurl, filename) {
        var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
            bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);

        while (n--) {
            u8arr[n] = bstr.charCodeAt(n);
        }

        return new File([u8arr], filename, { type: mime });
    }

    // <<< Funciones que no necesariamente se deben registrar en el $scope >>>
    // Obtiene los contratantes registrados por IDCliente
    function getContractor(ClientID) {
        $scope.gcOpenModalLoad();
        $scope.searchInput = "";
        historicServiceCpt.getCertificatesFUEC(ClientID, $scope.Token).then(function (responseData) {
            $scope.gridHistoric.data = responseData;
            $scope.gcCloseModalLoad();
        });
    }

    function getPDFBase(FUEC)
    {
        $scope.gcOpenModalLoad();
        $scope.getPDFFUEC = "";
        historicServiceCpt.getFUECBase(FUEC.ID, $scope.Token).then(function (responseData) {
            $scope.getPDFFUEC = responseData;
            $scope.gcCloseModalLoad();
            download("data:application/pdf;base64," + $scope.getPDFFUEC, 'CertificadoFUEC_' + FUEC.Plate + '_' + dateGenerate + '.pdf');
        });
    }
}]);
