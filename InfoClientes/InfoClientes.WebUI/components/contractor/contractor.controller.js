﻿//Se usa este comando para validar que la sintaxis se esta haciendo de la forma correcta
'use strict';

// Controlador del componente contratante
app.controller("contractorControllerCpt", ['$scope', 'contractorServiceCpt', '$mdDialog', '$filter', function ($scope, contractorServiceCpt, $mdDialog, $filter) {

    var today = new Date();
    $scope.selected = [];
    $scope.isNew = true;
    $scope.FunctionalityButton = "";
    $scope.AlfanumericValidateHyphen = new RegExp("^[-_\\w\\.\\ñÑ]+$", "i");
    $scope.AlfanumericValidate = new RegExp("^[wáéíóúÁÉÍÓÚ\\w\\s\\.\\ñÑ]+$", "i");
    $scope.ValidaronlyNumber= new RegExp("^([0-9])*$");
    $scope.DateInitial = new Date(today.getFullYear(), today.getMonth(), today.getDate());
    $scope.minDate = new Date(today.getFullYear(), today.getMonth(), today.getDate());
    $scope.gridContractor = { data: [{}]};
    $scope.objContractor = {};

    // <<< Inicializacion de funciones y metodos >>>
    $scope.gcOpenModalLoad();
    getContractor($scope.ClientID);

    // <<< Funciones registradas en el $scope >>>
    $scope.createContractor = function () {
        if ($scope.isNew) {
            if (!$scope.validateRegion1()) {
                saveContractor();
            }
        }
        else {
            if (!$scope.validateRegion1()) {  
                updateContractor();
                
            }
        }
    }

    // <<< Procesos para validar el formulario del módulo contratante >>>
    // Procesos del DataTable adaptado con angular js y angula rmaterial
    $scope.toggle = function (item, list) {
        var idx = list.indexOf(item);
        if (idx > -1) {
            list.splice(idx, 1);
        }
        else
        {
            list.push(item);
        }
    };

    $scope.isChecked = function () {
        return $scope.selected.length === $scope.gridContractor.data.length;
    };

    $scope.toggleAll = function () {
        if ($scope.selected.length === $scope.gridContractor.data.length) {
            $scope.selected = [];
        } else if ($scope.selected.length === 0 || $scope.selected.length > 0) {
            $scope.selected = $scope.gridContractor.data.slice(0);
        }
    };

    $scope.exists = function (item, list) {
        return list.indexOf(item) > -1;
    };

    $scope.editContractor = function (item) {
        $scope.objContractor.NIT = item.NIT;
        $scope.objContractor.CompanyName = item.CompanyName;
        $scope.objContractor.Contract = item.Contract;
        $scope.objContractor.ContractManager = item.ContractManager;
        $scope.objContractor.Identification = item.Identification;
        $scope.objContractor.Phone = item.Phone;
        $scope.objContractor.Address = item.Address;
        $scope.objContractor.BearingValidity = FormatDate(item.BearingValidity);
        $scope.objContractor.IDContractor = item.IDContractor;

        $scope.isNew = false;
        window.scrollTo(0, 0);
    }

    function CleanForm()
    {
        $scope.objContractor = {};
        $scope.contractorForm.$setPristine();
        $scope.contractorForm.$setUntouched();                
        $scope.selected = [];
        $scope.isNew = true;
    }

    $scope.deleteContractor = function (item) {
        var confirm = $mdDialog.confirm()
          .title('Está a punto de eliminar un contratante')
          .textContent('¿Desea eliminar el contratante seleccionado?')
          .ariaLabel('Ventana de confirmación')
          .ok('OK')
          .cancel('CANCELAR');
        $mdDialog.show(confirm).then(function () {
            deleteContractor(item);
        }, function () {});
    }

    $scope.UpdateBearing = function () {
        var dateBearingValidity = $filter('date')($scope.objContractor.UpdateBearingValidity, 'dd/MM/yyyy');

        if ($scope.selected.length > 0) {
            $scope.gcOpenModalLoad();
            $scope.objContractors = { Contractors: $scope.objContractor, BearingValidity: dateBearingValidity };
            // $scope.objContractor
            $scope.objContractors.Contractors = $scope.selected;
            contractorServiceCpt.updateBearingValidity($scope.objContractors, $scope.Token)
            .then(function (data) {
                if (data.Successfull) {
                    $scope.DisableConsecutive = true;
                    $scope.gcOpenToastSuccess(data.Messages);
                    getContractor($scope.ClientID);
                    CleanForm();
                }
                else {
                    $scope.gcCloseModalLoad();
                    $scope.gcOpenAlert(data.Messages);
                    getContractor($scope.ClientID);
                    CleanForm();
                }
            })
            .catch(function (data, status) {
                $scope.gcCloseModalLoad();
                console.error('Gists error', status, data);
                $scope.isNew = true;
            });
        }
        else{
            $scope.gcOpenAlert(['Se debe seleccionar por lo menos, un contratante de la tabla para realizar la actualización de la fecha de vigencia de rodamiento.']);
        }
    }

    // Exportar a EXCEL
    $scope.exportContrator = function () {
        // Encabezado
        var arrHeadDataExport = [];
        arrHeadDataExport.push(["NIT", "Nombre/Raz&oacute;n Social", "Contrato", "Responsable Contrato", "Identificaci&oacute;n", "Tel&eacute;fono", "Direcci&oacute;n", "Vigencia Rodamiento"]);

        // Información de la tabla
        var arrDataExport = [];

        angular.forEach($scope.gridContractor.data, function (value, key) {
            arrDataExport.push([value.NIT, value.CompanyName, value.Contract, value.ContractManager, value.Identification, value.Phone, value.Address, value.BearingValidity]);
        });

        var blob = new Blob(["\ufeff", getSheet(arrHeadDataExport, arrDataExport)], {
            type: "application/vnd.ms-excel;charset=utf-8;"
        });

       saveAs(blob, "Reporte_Contratantes.xls");
    }

    // Esta funcion valida los campos indicados para el registro y la actualizacion del contratante
    // Retorna true siempre y cuando todo lo diligenciado sea correcto
    $scope.validateRegion1 = function () {
        if ($scope.contractorForm.NIT.$valid == true &&
            $scope.contractorForm.companyName.$valid == true &&
            $scope.contractorForm.Contract.$valid == true &&
            $scope.contractorForm.ContractManager.$valid == true &&
            $scope.contractorForm.Identification.$valid == true &&
            $scope.contractorForm.Phone.$valid == true &&
            $scope.contractorForm.Address.$valid == true &&
            $scope.contractorForm.BearingValidity.$valid == true) {
            return false;
        }
        else {
            return true;
        }
    }

    $scope.validateRegion2 = function () {
        if ($scope.contractorForm.UpdateBearingValidity.$valid == true) {
            return false;
        }
        else {
            return true;
        }
    }

    // <<< Funciones que no necesariamente se deben registrar en el $scope >>>
    // Obtiene los contratantes registrados por IDCliente
    function getContractor(ClientID) {
        $scope.searchInput = "";
        contractorServiceCpt.getContractorsGrid(ClientID, $scope.Token).then(function (responseData) {
            $scope.gcCloseModalLoad();
            $scope.gridContractor.data = responseData;
        });
    }

    // Registra el contratante 
    function saveContractor() {
        $scope.gcOpenModalLoad();
        var obj = setobjContractor($scope.objContractor);
        contractorServiceCpt.registerContractor(obj, $scope.Token)
        .then(function (data) {
            if (data.Successfull) {
                $scope.DisableConsecutive = true;
                $scope.gcOpenToastSuccess(data.Messages);
                $scope.isNew = true;
                // Actualizamos Grid de contratantes
                CleanForm();
                getContractor($scope.ClientID);                
            }
            else {
                $scope.gcCloseModalLoad();
                $scope.gcOpenAlert(data.Messages);
                $scope.isNew = true;
            }
        })
        .catch(function (data, status) {
            $scope.gcCloseModalLoad();
            console.error('Gists error', status, data);
            $scope.isNew = true;
        });
    }

    function setobjContractor(objContractor)
    {
        var objContractorNew = {};
        objContractorNew.ClientID = $scope.ClientID;
        objContractorNew.IDContractor = objContractor.IDContractor;
        objContractorNew.NIT = objContractor.NIT;
        objContractorNew.CompanyName = objContractor.CompanyName;
        objContractorNew.Contract = objContractor.Contract;
        objContractorNew.ContractManager = objContractor.ContractManager;
        objContractorNew.Identification = objContractor.Identification;
        objContractorNew.Phone = objContractor.Phone;
        objContractorNew.Address = objContractor.Address;
        objContractorNew.BearingValidity = $filter('date')(objContractor.BearingValidity, 'dd/MM/yyyy');
        objContractorNew.IDContractor = objContractor.IDContractor;

        return objContractorNew;
    }

    function updateContractor()
    {
        $scope.gcOpenModalLoad();
        var obj = setobjContractor($scope.objContractor);
        contractorServiceCpt.updateContractor(obj, $scope.Token)
        .then(function (data) {
            if (data.Successfull) {
                $scope.DisableConsecutive = true;
                $scope.gcOpenToastSuccess(data.Messages);
                $scope.isNew = true;
                getContractor($scope.ClientID);
                CleanForm();
            }
            else {
                $scope.gcCloseModalLoad();
                $scope.gcOpenAlert(data.Messages);
                $scope.isNew = true;
                getContractor($scope.ClientID);
            }
        })
        .catch(function (data, status) {
            $scope.gcCloseModalLoad();
            console.error('Gists error', status, data);
            $scope.isNew = true;
            CleanForm();
        });
    }

    function deleteContractor(contractor) {
        $scope.gcOpenModalLoad();
        var obj = setobjContractor(contractor);
        contractorServiceCpt.deleteContractor(obj, $scope.Token)
        .then(function (data) {
            if (data.Successfull) {
                $scope.DisableConsecutive = true;
                $scope.gcOpenToastSuccess(data.Messages);
                $scope.isNew = true;
                getContractor($scope.ClientID);
                CleanForm();
            }
            else {
                $scope.gcCloseModalLoad();
                $scope.gcOpenAlert(data.Messages);
                $scope.isNew = true;
                getContractor($scope.ClientID);
            }
        })
        .catch(function (data, status) {
            $scope.gcCloseModalLoad();
            console.error('Gists error', status, data);
            $scope.isNew = true;
            CleanForm();
        });
    }

    // Exporta a EXCEL la tabla de contratantes
    function getSheet(dataHead, data) {
        var exportTable = "<table>";
        exportTable += "<thead><tr>";
        for (var R = 0; R != dataHead.length; ++R) {
            for (var C = 0; C != dataHead[R].length; ++C) {
                exportTable += "<th style='background-color: #253081; color: white; width: 200px; height: 40px; vertical-align: middle'>" + dataHead[R][C] + "</th>";
            }
        }
        exportTable += "</tr></thead>";

        exportTable += "<tbody>";
        for (var R = 0; R != data.length; ++R) {
            exportTable += "<tr>";
            for (var C = 0; C != data[R].length; ++C) {
                exportTable += "<td  style='text-align: center; border: 1px solid #eee; height: 40px; vertical-align: middle'>" + data[R][C] + "</td>";
            }
            exportTable += "</tr>";
        }
        exportTable += "</tbody>";
        exportTable += "</table>";

        return exportTable;
    };
}]);

