﻿// Se usa este comando para validar que la sintaxis se esta haciendo de la forma correcta
'use strict';

// Servicio del componente contratante
app.factory('contractorServiceCpt', function ($http, $q, $httpParamSerializerJQLike, urlApi, $filter) {
    return {
        registerContractor: function (objContractor, token) {   
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: urlApi.Contractor.metPostSaveContractor,
                dataType: "json",
                contentType: "application/json",                
                data: JSON.stringify(objContractor),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': token
                },
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                deferred.resolve(response.data);
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                deferred.resolve()
            });
            return deferred.promise;
        },

        getContractorsGrid: function (ClientID, token) {

            var deferred = $q.defer();
            $http({
                method: 'GET',
                dataType: "JSON",
                url: urlApi.Contractor.metGetContractors + ClientID,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': token
                },
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                // when the response is available

                //response.data = mockTest;
                deferred.resolve(response.data);

            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                deferred.resolve()
            });
            
            //deferred.resolve(mockData);
            return deferred.promise;
        },

        updateContractor: function (objContractor, token) {
            //var formatDate = $filter('date')(objContractor.BearingValidity, 'dd/MM/yyyy');
            //objContractor.BearingValidity = formatDate;

            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: urlApi.Contractor.metPutContractor,
                dataType: "json",
                contentType: "application/json",
                data: JSON.stringify(objContractor),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': token
                },
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                deferred.resolve(response.data);
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                deferred.resolve()
            });
            return deferred.promise;
        },

        updateBearingValidity: function (Contractors, token) {

            var deferred = $q.defer();

            $http({
                method: 'POST',
                url: urlApi.Contractor.metUpdateBearingValidity,
                dataType: "json",
                contentType: 'application/json',
                data: JSON.stringify(Contractors),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': token
                },
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                deferred.resolve(response.data);
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                deferred.resolve()
            });
            return deferred.promise;
        },

        deleteContractor: function (objContractor, token) {

            var deferred = $q.defer();

            $http({
                method: 'POST',                
                url: urlApi.Contractor.metDeleteContractor,
                dataType: "json",
                contentType: 'application/json',
                data: JSON.stringify(objContractor),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': token
                },
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                deferred.resolve(response.data);
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                deferred.resolve()
            });
            return deferred.promise;
        }
     };            
});

