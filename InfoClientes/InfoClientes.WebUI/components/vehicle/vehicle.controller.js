﻿//Se usa este comando para validar que la sintaxis se esta haciendo de la forma correcta
'use strict';

// Controlador del componente vehículo
app.controller("vehicleControllerCpt", ['$scope', 'vehicleServiceCpt', '$mdDialog', '$q', '$timeout', '$filter', function ($scope, vehicleServiceCpt, $mdDialog, $q, $timeout, $filter) {
    var today = new Date();
    $scope.minDate = new Date(today.getFullYear(), today.getMonth(), today.getDate());
    $scope.objVehicle = {};
    $scope.arrVehicles = [];
    $scope.isNew = true;
    $scope.gridVehicle = { data: [] };

    // <<< Inicializacion de funciones y metodos >>>
    Init();

    // <<< Funciones registradas en el $scope >>> 
    // Esta funcion valida los campos indicados para el registro y la actualizacion del contratante
    // Retorna true siempre y cuando todo lo diligenciado sea correcto
    $scope.validateRegion1 = function () {
        if ($scope.vehicleForm.Model.$valid == true &&
            $scope.vehicleForm.Trademark.$valid == true &&
            $scope.vehicleForm.Type.$valid == true &&
            $scope.vehicleForm.InternalNumber.$valid == true &&
            $scope.vehicleForm.OperationCard.$valid == true &&
            $scope.vehicleForm.TechnicalReviewValidity.$valid == true) {
            return false;
        }
        else {
            return true;
        }
    }

    $scope.validateRegion1Submit = function () {
        $scope.vehicleForm.InternalNumber.$valid == true 
        $scope.vehicleForm.OperationCard.$valid == true 
        $scope.vehicleForm.TechnicalReviewValidity.$valid == true
    }

    // Agrega efecto de espera al autocompletado
    $scope.querySearch = function (searchText) {
        var plates = searchText != "" ? $scope.arrVehicles.filter(function (plate) {
            return (plate.toUpperCase().indexOf(searchText.toUpperCase()) !== -1);
        }) : $scope.arrVehicles, deferred;
        deferred = $q.defer();
        $timeout(function () {
            deferred.resolve(plates);
        }, Math.random() * 1000, false);

        return deferred.promise;
    }

    // Asigna el valor seleccionado del autocompletar al ng model encargado de obtener la info del vehìculo
    $scope.setModelPlates = function (item) {
        if (item != null) {
            getVehicle($scope.ClientID, item);
        } else
        {
            CleanForm();
        }
    }

    $scope.createVehicle = function () {
        if (!$scope.validateRegion1()) {
            if ($scope.isNew) {
                saveVehicle();
            }
            else {
                updateVehicle();
            }
        }
    }

    $scope.validateAllianceChange = function () {
        if ($scope.objVehicle.AllianceType == "Seleccionar") {
            $scope.objVehicle.AllianceType = null;
            $scope.objVehicle.AllianceWith = "";
        }
    }

    // Exportar a EXCEL
    $scope.exportContrator = function () {
        // Encabezado
        var arrHeadDataExport = [];
        arrHeadDataExport.push(["Placa", "Modelo", "Marca", "Clase", "N&uacute;mero interno", "Tarjeta operaci&oacute;n", "Vigencia revisi&oacute;n t&eacute;cnica", "Alianza"]);

        // Información de la tabla
        var arrDataExport = [];

        angular.forEach($scope.gridVehicle.data, function (value, key) {

            arrDataExport.push([value.Plate, value.Model, value.Trademark, value.Type, value.InternalNumber, value.OperationCard, value.TechnicalReviewValidity, value.AllianceType]);
        });

        var blob = new Blob(["\ufeff", getSheet(arrHeadDataExport, arrDataExport)], {
            type: "application/vnd.ms-excel;charset=utf-8;"
        });
        
        saveAs(blob, "Reporte_Vehiculo.xls");
    }

    // <<< Funciones que no necesariamente se deben registrar en el $scope >>>
    // Inicialización de funciones
    function Init() {
        $scope.gcOpenModalLoad();
        getVehiclesPlate($scope.ClientID);
        getVehicles($scope.ClientID);
    }

    // Obtiene los vehículos por IDCliente
    function getVehiclesPlate(ClientID) {
        vehicleServiceCpt.getVehiclesPlate(ClientID, $scope.Token)
        .then(function (data) {
            
            $scope.arrVehicles = data;
        })
        .catch(function (data, status) {
            console.error('Gists error', status, data);
        });
    }

    function getVehicle(ClientID, PhysicalID) {        
        vehicleServiceCpt.getVehicle(ClientID, PhysicalID, $scope.Token)
        .then(function (data) {
            var format = formatDate(data.TechnicalReviewValidity);
            data.TechnicalReviewValidity = format;
            var modelValue = data.Model;
            data.Model = modelValue == 0 ? null : data.Model;
            $scope.objVehicle = data;

            // Si ClientID es 0, quiere decir que es un nuevo registro
            if (data.ClientID != 0) {
                $scope.isNew = false;
            }

            // Validamos que la fecha sea actual o superior
            if ($scope.objVehicle.TechnicalReviewValidity) {
                $scope.vehicleForm.TechnicalReviewValidity.$setTouched();
            }
            else {
                $scope.vehicleForm.TechnicalReviewValidity.$setUntouched();
            }
        })
        .catch(function (data, status) {
            console.error('Gists error', status, data);
        });
    }

    function getVehicles(ClientID) {
        vehicleServiceCpt.getVehicles(ClientID, $scope.Token)
        .then(function (data) {
            var arrVehiclesSortable = sortProperties(data);           
            $scope.gcCloseModalLoad();
            $scope.gridVehicle.data = arrVehiclesSortable;
        })
        .catch(function (data, status) {
            $scope.gcCloseModalLoad();
            console.error('Gists error', status, data);
        });
    }
    ///Funcion encargada de ordenar por placa en la primera carga
    function sortProperties(obj) {
        // convert object into array
        var sortable = [];
        var objVehicle = {};
        var arrVehiclesSortable = [];

        for (var key in obj)
            if (obj.hasOwnProperty(key))
                sortable.push([key, obj[key]]); // each item is an array in format [key, value]

        // sort items by Plate
        sortable.sort(function (a, b) {
            var x = a[1].Plate.toLowerCase(),
                y = b[1].Plate.toLowerCase();
            return x < y ? -1 : x > y ? 1 : 0;
        });

        angular.forEach(sortable, function (value, key) {
            objVehicle = value[1];
            arrVehiclesSortable.push(objVehicle);
        });

        return arrVehiclesSortable; // array in format [ [ key1, val1 ], [ key2, val2 ], ... ]
    }

    // Registra el vehículo 
    function saveVehicle() {
        $scope.gcOpenModalLoad();
        //var objCast = {};
        //objCast.ClientID = $scope.ClientID;
        //objCast.VehicleID = $scope.objVehicle.VehicleID;
        //objCast.Plate = $scope.objVehicle.Plate;
        //objCast.Model = $scope.objVehicle.Model;
        //objCast.Trademark = $scope.objVehicle.Trademark;
        //objCast.Type = $scope.objVehicle.Type;
        //objCast.InternalNumber = $scope.objVehicle.InternalNumber;
        //objCast.OperationCard = $scope.objVehicle.OperationCard;
        //objCast.TechnicalReviewValidity = $filter('date') (Date.parse($scope.objVehicle.TechnicalReviewValidity), 'dd/MM/yyyy');
        //objCast.AllianceType = $scope.objVehicle.AllianceType;
        //objCast.AllianceWith = $scope.objVehicle.AllianceWith;

        var objCast = setobjVehicle($scope.objVehicle);
        
        vehicleServiceCpt.registerVehicle(objCast, $scope.Token)
        .then(function (data) {
            if (data.Successfull) {
                $scope.gcOpenToastSuccess([data.Messages[0] + " " + $scope.searchTextPlates]);
                CleanForm();
            }
            else {
                $scope.gcOpenAlert(data.Messages);
            }

            //Actualizamos la Grid de vehículos
            getVehicles($scope.ClientID);
        })
        .catch(function (data, status) {
            $scope.gcCloseModalLoad();
            console.error('Gists error', status, data);
        });
    }

    // Actualiza el vehículo 
    function updateVehicle() {
        $scope.gcOpenModalLoad();
        //$scope.objVehicle.ClientID = $scope.ClientID;
        var objCast = setobjVehicle($scope.objVehicle);
        vehicleServiceCpt.updateVehicle(objCast, $scope.Token)
        .then(function (data) {
            if (data.Successfull) {
                $scope.gcOpenToastSuccess([data.Messages[0] + " " + $scope.searchTextPlates]);

                CleanForm();
            }
            else {
                $scope.gcOpenAlert(data.Messages);
            }

            //Actualizamos la Grid de vehículos
            getVehicles($scope.ClientID);
        })
        .catch(function (data, status) {
            console.error('Gists error', status, data);
        });
    }

    // Limpia el formulario
    function CleanForm() {
        $scope.searchTextPlates = "";
        $scope.searchItemPlate = null;
        $scope.objVehicle = {};
        $scope.vehicleForm.$setPristine();
        $scope.vehicleForm.$setUntouched();
        $scope.isNew = true;
    }

    // Función para castear el formato de fechas del aplicativo
    function formatDate(originalDate) {
        if (originalDate == null || originalDate == "") {
            return null;
        }
        else {
            var s = originalDate + " 00:00";
            var bits = s.split(/\D/);
            var date = new Date(bits[2], --bits[1], bits[0], bits[3], bits[4]);

            return date;
        }
    }

    function setobjVehicle(objVehicle) {


        var objVehicleNew = {};
        objVehicleNew.ClientID = $scope.ClientID;
        objVehicleNew.VehicleID = objVehicle.VehicleID;
        objVehicleNew.Plate = objVehicle.Plate;
        objVehicleNew.Model = objVehicle.Model;
        objVehicleNew.Trademark = objVehicle.Trademark;
        objVehicleNew.Type = objVehicle.Type;
        objVehicleNew.InternalNumber = objVehicle.InternalNumber;
        objVehicleNew.OperationCard = objVehicle.OperationCard;
        objVehicleNew.TechnicalReviewValidity = $filter('date')(objVehicle.TechnicalReviewValidity, 'dd/MM/yyyy');
        objVehicleNew.AllianceType = objVehicle.AllianceType;
        objVehicleNew.AllianceWith = objVehicle.AllianceWith;

        return objVehicleNew;
    }

    // Exporta a EXCEL la tabla de vehiculos
    function getSheet(dataHead, data) {
        var exportTable = "<table>";
        exportTable += "<thead><tr>";
        for (var R = 0; R != dataHead.length; ++R) {
            for (var C = 0; C != dataHead[R].length; ++C) {
                exportTable += "<th style='background-color: #253081; color: white; width: 200px; height: 40px; vertical-align: middle'>" + dataHead[R][C] + "</th>";
            }
        }
        exportTable += "</tr></thead>";

        exportTable += "<tbody>";
        for (var R = 0; R != data.length; ++R) {
            exportTable += "<tr>";
            for (var C = 0; C != data[R].length; ++C) {
                exportTable += "<td  style='text-align: center; border: 1px solid #eee; height: 40px; vertical-align: middle'>" + data[R][C] + "</td>";
            }
            exportTable += "</tr>";
        }
        exportTable += "</tbody>";
        exportTable += "</table>";

        return exportTable;
    };
}]);

