﻿//Se usa este comando para validar que la sintaxis se esta haciendo de la forma correcta
'use strict';

app.controller("driverControllerCpt", ['$scope', 'clientServiceCpt', '$mdDialog', '$filter', function ($scope, clientServiceCpt, $mdDialog, $filter) {
    
    // <<< Inicializacion de funciones y metodos >>>
    Init();

    // <<< Funciones registradas en el $scope >>> 
    // Esta funcion valida los campos indicados para el registro y la actualizacion del conductor
    // Retorna true siempre y cuando todo lo diligenciado sea correcto
    $scope.validateRegion1 = function () {
        if ($scope.driverForm.Name.$valid == true &&
            $scope.driverForm.Identification.$valid == true &&
            $scope.driverForm.LicenseNumber.$valid == true &&
            $scope.driverForm.LicenseValidity.$valid == true) {
            return false;
        }
        else {
            return true;
        }
    }

    $scope.createDriver = function () {
        if (!$scope.validateRegion1()) {
            if ($scope.isNew) {
                saveDriver();
            }
            else {
                updateDriver();
            }
        }
    }

    $scope.editDriver = function (item) {
        $scope.objDriver.Name = item.Name;
        $scope.objDriver.Identification = item.Identification;
        $scope.objDriver.LicenseNumber = item.LicenseNumber;
        $scope.objDriver.LicenseValidity =  FormatDate(item.LicenseValidity);
        $scope.objDriver.DriverID = item.DriverID;
        $scope.isNew = false;
        window.scrollTo(0, 0);
    }

    // <<< Procesos para validar el formulario del módulo contratante >>>
    // Procesos del DataTable adaptado con angular js y angula rmaterial
    $scope.toggle = function (item, list) {
        var idx = list.indexOf(item);
        if (idx > -1) {
            list.splice(idx, 1);
        }
        else {
            list.push(item);
        }
    };

    //Elimina un conductor
    $scope.deleteDriver = function (item) {
        var confirm = $mdDialog.confirm()
          .title('Está a punto de eliminar un conductor')
          .textContent('¿Desea eliminar el conductor seleccionado?')
          .ariaLabel('Ventana de confirmación')
          .ok('OK')
          .cancel('CANCELAR');
        $mdDialog.show(confirm).then(function () {
            deleteDriver(item);
        }, function () { });
    }
    
    // <<< Funciones que no necesariamente se deben registrar en el $scope >>>
    // Inicialización de funciones
    function Init() {
        var today = new Date();
        $scope.minDate = new Date(today.getFullYear(), today.getMonth(), today.getDate());
        $scope.objDriver = { DriverObject: [] };
        $scope.arrobjDrivers = [];
        $scope.gridDriver = { data: [] };
        $scope.Name = "";
        $scope.Identification = "";
        $scope.LicenseNumber = "";
        $scope.DriverID = 0;
        $scope.ValidarAlfanumerico = new RegExp("^[wáéíóúÁÉÍÓÚ\\w\\s\\.\\ñÑ]+$", "i");
        $scope.isNew = true;
        $scope.searchInput = "";
        $scope.gcOpenModalLoad();
        getDrivers($scope.ClientID);
    }

    function getDrivers(ClientID) {
        driverServiceCpt.getDrivers(ClientID, $scope.Token)
        .then(function (data) {
            $scope.gcCloseModalLoad();
            $scope.gridDriver.data = data;
        })
        .catch(function (data, status) {
            $scope.gcCloseModalLoad();
            console.error('Gists error', status, data);
        });
    }

    // Registra el conductor 
    function saveDriver() {
        $scope.gcOpenModalLoad();
        var objDriver = setobjDriver($scope.objDriver);
        driverServiceCpt.registerDriver(objDriver, $scope.Token)
        .then(function (data) {
            if (data.Successfull) {
                $scope.gcOpenToastSuccess([data.Messages[0]]);
                CleanForm();
            }
            else {
                $scope.gcOpenAlert(data.Messages);
            }

            //Actualizamos la Grid de vehículos
            getDrivers($scope.ClientID);
        })
        .catch(function (data, status) {
            $scope.gcCloseModalLoad();
            console.error('Gists error', status, data);
        });
    }

    //Actualización de conductor
    function updateDriver() {
        $scope.gcOpenModalLoad();
        var objDriver = setobjDriver($scope.objDriver);
        driverServiceCpt.updateDriver(objDriver, $scope.Token)
        .then(function (data) {
            if (data.Successfull) {
                $scope.DisableConsecutive = true;
                $scope.gcOpenToastSuccess(data.Messages);
                $scope.isNew = true;
                getDrivers($scope.ClientID);
                CleanForm();
            }
            else {
                $scope.gcCloseModalLoad();
                $scope.gcOpenAlert(data.Messages);
            }
        })
        .catch(function (data, status) {
            console.error('Gists error', status, data);
            $scope.isNew = true;
            CleanForm();
            $scope.gcCloseModalLoad();
        });
    }

    //Eliminar conductor
    function deleteDriver(driver) {
        $scope.gcOpenModalLoad();
        driverServiceCpt.deleteDriver(driver.DriverID, $scope.Token)
        .then(function (data) {
            if (data.Successfull) {
                $scope.DisableConsecutive = true;
                $scope.gcOpenToastSuccess(data.Messages);
                $scope.isNew = true;
                getDrivers($scope.ClientID);
                CleanForm();
            }
            else {
                $scope.gcCloseModalLoad();
                $scope.gcOpenAlert(data.Messages);
            }
        })
        .catch(function (data, status) {
            $scope.gcCloseModalLoad();
            console.error('Gists error', status, data);
            $scope.isNew = true;
            CleanForm();
        });
    }
   
    // Limpia el formulario
    function CleanForm() {
        $scope.searchTextDrivers = "";
        $scope.searchItemDriver = null;
        $scope.objDriver = {};
        $scope.driverForm.$setPristine();
        $scope.driverForm.$setUntouched();
        $scope.isNew = true;
    }

    function setobjDriver(objDriver) {


        var objDriverNew = {};
        objDriverNew.ClientID = $scope.ClientID;
        objDriverNew.Name = objDriver.Name;
        objDriverNew.Identification = objDriver.Identification;
        objDriverNew.LicenseNumber = objDriver.LicenseNumber;;
        objDriverNew.LicenseValidity = $filter('date')(objDriver.LicenseValidity, 'dd/MM/yyyy');
        objDriverNew.DriverID = objDriver.DriverID;

        return objDriverNew;
    }


    
}]);

