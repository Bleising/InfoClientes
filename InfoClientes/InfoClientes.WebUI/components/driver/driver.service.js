﻿// Se usa este comando para validar que la sintaxis se esta haciendo de la forma correcta
'use strict';


// Servicio del componente empresa
app.factory('driverServiceCpt', function ($http, $q, $httpParamSerializerJQLike, urlApi, $filter) {
    return {
       
        getDrivers: function (ClientID, token) {
            var deferred = $q.defer();
            $http({
                method: 'GET',
                dataType: "JSON",
                url: urlApi.driver.metGetAllDrivers + ClientID,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': token
                },
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                // when the response is available
                deferred.resolve(response.data);
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                deferred.resolve()
            });
            return deferred.promise;
        },
        registerDriver: function (objDriver, token) {
            var formatDate = $filter('date')(objDriver.LicenseValidity, 'dd/MM/yyyy');
            objDriver.LicenseValidity = formatDate;
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: urlApi.driver.metPostSaveDriver,
                dataType: "json",
                contentType: "application/json charset=utf-8",
                data: JSON.stringify(objDriver),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': token
                },
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                deferred.resolve(response.data);
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                deferred.resolve()
            });
            return deferred.promise;
        },
        updateDriver: function (objDriver, token) {
            var formatDate = $filter('date')(objDriver.LicenseValidity, 'dd/MM/yyyy');
            objDriver.LicenseValidity = formatDate;
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: urlApi.driver.metPutDriver,
                dataType: "json",
                contentType: "application/json charset=utf-8",
                data: JSON.stringify(objDriver),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': token
                },
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                deferred.resolve(response.data);
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                deferred.resolve()
            });
            return deferred.promise;
        },
        deleteDriver: function (DriverID, token) {
            var deferred = $q.defer();
            $http({
                method: 'POST',
                dataType: "json",
                contentType: 'application/json',
                url: urlApi.driver.metDeleteDriver + DriverID,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': token
                },
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                deferred.resolve(response.data);
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                deferred.resolve()
            });

            return deferred.promise;
        },
    };
})
