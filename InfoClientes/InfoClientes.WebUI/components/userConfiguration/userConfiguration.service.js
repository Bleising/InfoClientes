﻿// Se usa este comando para validar que la sintaxis se esta haciendo de la forma correcta
'use strict';

// Servicio del componente contratante
app.factory('userConfigurationServiceCpt', function ($http, $q, $httpParamSerializerJQLike, urlApi, $filter) {
    return {
        postSaveUserRole: function (lstUserRole, token) {

            var deferred = $q.defer();
            $http({

                method: 'POST',
                url: urlApi.usersRole.metSaveUsers,
                dataType: "json",
                contentType: 'application/json',
                data: JSON.stringify(lstUserRole.lstUserRole),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': token
                },
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                // when the response is available

                deferred.resolve(response.data);

            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                deferred.resolve()
            });

            //deferred.resolve(mockData);
            return deferred.promise;
        },
        getUserRollsGrid: function (token) {

            var deferred = $q.defer();
            $http({
                method: 'GET',
                dataType: "JSON",
                url: urlApi.usersRole.metGetUserRoles,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': token
                },
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                // when the response is available

                deferred.resolve(response.data);

            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                deferred.resolve()
            });
            return deferred.promise;
        },
        getUser: function (user, token) {
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url:  urlApi.usersRole.metGetUsers,
                dataType: "json",
                contentType: 'application/json',
                data: JSON.stringify(user),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': token
                },
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                // when the response is available

                deferred.resolve(response.data);

            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                deferred.resolve()
            });
            return deferred.promise;
        },
        getUserPermission: function (User, token) {
            var deferred = $q.defer();
            $http({
                method: 'GET',
                dataType: "JSON",
                contentType: 'application/json',
                url: urlApi.usersRole.metGetUserPermission,
                data: JSON.stringify(User),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': token
                },
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                // when the response is available

                deferred.resolve(response.data);

            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                deferred.resolve()
            });
            return deferred.promise;
        },
    }
});

