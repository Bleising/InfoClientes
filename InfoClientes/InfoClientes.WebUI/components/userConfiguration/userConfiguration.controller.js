﻿//Se usa este comando para validar que la sintaxis se esta haciendo de la forma correcta
'use strict';

// Controlador del componente Certificado
app.controller("userConfigurationControllerCpt", ['$scope', 'userConfigurationServiceCpt', '$mdDialog', '$q', '$timeout', '$filter', function ($scope, userConfigurationServiceCpt, $mdDialog, $q, $timeout, $filter) {
    $scope.RoleUsers = [{}];
    $scope.myDataUsers = [{}];
    $scope.UserRoll = [];
    $scope.lstUserRole = {};
    $scope.gridUserRole = {
        data: $scope.myDataUsers
    };

    Init();

    $scope.SaveUserRoles = function () {
        $scope.lstUserRole = { lstUserRole: $scope.gridUserRole.data };
        $scope.gcOpenModalLoad();

        userConfigurationServiceCpt.postSaveUserRole($scope.lstUserRole, $scope.Token)
        .then(function (data) {
            if (data.Successfull) {
                $scope.gcCloseModalLoad();
                $scope.gcOpenToastSuccess(data.Messages);
            }
            else {
                $scope.gcCloseModalLoad();
                $scope.gcOpenAlert(data.Messages);
            }
        });
    }

    function Init() {
        $scope.gcOpenModalLoad();
        getUserRoles();
        getUsers();
    }

    function getUserRoles() {
        userConfigurationServiceCpt.getUserRollsGrid($scope.Token)
        .then(function (data) {
            $scope.RoleUsers = data;
        })
         .catch(function (data, status) {
             console.error('Gists error', status, data);
         });
    }

    function getUsers() {
        var User = [];
        User = { ClientID: $scope.ClientID, UserName: $scope.UserPage,  RoleID: 0 };

        userConfigurationServiceCpt.getUser(User, $scope.Token)
        .then(function (data) {
            $scope.gridUserRole.data = data;
            $scope.gcCloseModalLoad();
        })
        .catch(function (data, status) {
            $scope.gcCloseModalLoad();
            console.error('Gists error', status, data);
        });
    }
}]);