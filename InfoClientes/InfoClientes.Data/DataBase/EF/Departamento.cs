//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace InfoClientes.Data.DataBase.EF
{
    using Core.Entities;
    using System;
    using System.Collections.Generic;

    public partial class Departamento : Entity
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Departamento()
        {
            this.Ciudad = new HashSet<Ciudad>();
        }
    
        public int IDDepartamento { get; set; }
        public string NombreDepartamento { get; set; }
        public int PaisID { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Ciudad> Ciudad { get; set; }
        public virtual Pais Pais { get; set; }
    }
}
