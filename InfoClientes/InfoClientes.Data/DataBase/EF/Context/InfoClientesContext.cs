﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfoClientes.Data.DataBase.EF.Context
{
    public class InfoClientesContext : ObjectContext
    {
        private ObjectSet<Ciudad> _Ciudad;
        private ObjectSet<Cliente> _Cliente;
        private ObjectSet<Departamento> _Departamento;
        private ObjectSet<Pais> _Pais;
        private ObjectSet<Vendedor> _Vendedor;
        private ObjectSet<Visita> _Visita;

        public ObjectSet<Ciudad> Ciudad
        {
            get { return _Ciudad; }
        }
        public ObjectSet<Cliente> Cliente
        {
            get { return _Cliente; }
        }
        public ObjectSet<Departamento> Departamento
        {
            get { return _Departamento; }
        }
        public ObjectSet<Pais> Pais
        {
            get { return _Pais; }
        }
        public ObjectSet<Vendedor> Vendedor
        {
            get { return _Vendedor; }
        }
        public ObjectSet<Visita> Visita
        {
            get { return _Visita; }
        }

        /// <summary>
        /// Crea un contexto basado en el modelo definido y la conexión de baes de datos indicada
        /// </summary>
        public InfoClientesContext() :
            base("name="+ ConfigurationManager.AppSettings["ContextoInfoClientes"], ConfigurationManager.AppSettings["ContextoInfoClientes"]) //ConfigurationManager.AppSettings["ContextoInfoClientes"]
        {
            _Ciudad = CreateObjectSet<Ciudad>();
            _Cliente = CreateObjectSet<Cliente>();
            _Departamento = CreateObjectSet<Departamento>();
            _Pais = CreateObjectSet<Pais>();
            _Vendedor = CreateObjectSet<Vendedor>();
            _Visita = CreateObjectSet<Visita>();
            base.ContextOptions.LazyLoadingEnabled = true;
        }
    }
}
