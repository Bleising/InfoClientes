﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace InfoClientes.Data.DataBase.EF.Context
{
    public static class ObjectContextManager 
    {

        private static readonly Hashtable _threadObjectContexts = new Hashtable();
        private const string OBJECT_CONTEXT_KEY = "InfoClientes.Data.DataBase.Repositories.ObjectContext";

        /// <summary>
        /// Crea un contexto
        /// </summary>
        public static ObjectContext GetInfoClientesObjectContext()
        {
            ObjectContext objectContext = GetCurrentObjectContext();
            if (objectContext == null || !(objectContext is InfoClientesContext))
            {
                objectContext = new InfoClientesContext();
                StoreCurrentObjectContext(objectContext);
            }
            return objectContext;
        }
        /// <summary>
        /// Setea un contexto
        /// </summary>
        /// <param name="repositoryContext">Objeto que representa un contexto</param>
        public static void SetRepositoryContext(object repositoryContext)
        {
            if (repositoryContext == null)
            {
                RemoveCurrentObjectContext();
            }
            else if (repositoryContext is ObjectContext)
            {
                StoreCurrentObjectContext((ObjectContext)repositoryContext);
            }
        }

        #region Object Context Lifecycle Management

        /// <summary>
        /// Devuelve el contexto actual 		
        /// </summary>
        private static ObjectContext GetCurrentObjectContext()
        {
            ObjectContext objectContext = null;
            if (HttpContext.Current == null)
                objectContext = GetCurrentThreadObjectContext();
            else
                objectContext = GetCurrentHttpContextObjectContext();
            return objectContext;
        }

        /// <summary>
        /// Establece la sessión actual del contexto 		
        /// </summary>
        private static void StoreCurrentObjectContext(ObjectContext objectContext)
        {
            if (HttpContext.Current == null)
                StoreCurrentThreadObjectContext(objectContext);
            else
                StoreCurrentHttpContextObjectContext(objectContext);
        }

        /// <summary>
        /// Remueve el contexto actual 		
        /// </summary>
        private static void RemoveCurrentObjectContext()
        {
            if (HttpContext.Current == null)
                RemoveCurrentThreadObjectContext();
            else
                RemoveCurrentHttpContextObjectContext();
        }

        #region private methods - HttpContext related

        /// <summary>
        /// Devuelve el contexto para el hilo actual 
        /// </summary>
        private static ObjectContext GetCurrentHttpContextObjectContext()
        {
            ObjectContext objectContext = null;
            if (HttpContext.Current.Items.Contains(BuildHttpContextName()))
                objectContext = (ObjectContext)HttpContext.Current.Items[BuildHttpContextName()];
            return objectContext;
        }

        private static void StoreCurrentHttpContextObjectContext(ObjectContext objectContext)
        {
            if (HttpContext.Current.Items.Contains(BuildHttpContextName()))
                HttpContext.Current.Items[BuildHttpContextName()] = objectContext;
            else
                HttpContext.Current.Items.Add(BuildHttpContextName(), objectContext);
        }

        /// <summary>
        /// Elimina la sesión del contexto actual
        /// </summary>
        private static void RemoveCurrentHttpContextObjectContext()
        {
            ObjectContext objectContext = GetCurrentHttpContextObjectContext();
            if (objectContext != null)
            {
                HttpContext.Current.Items.Remove(BuildHttpContextName());
                objectContext.Dispose();
            }
        }

        #endregion

        #region private methods - ThreadContext related

        /// <summary>
        /// Devuelve la sesión del del hilo actual
        /// </summary>
        private static ObjectContext GetCurrentThreadObjectContext()
        {
            ObjectContext objectContext = null;
            Thread threadCurrent = Thread.CurrentThread;
            if (threadCurrent.Name == null)
                threadCurrent.Name = Guid.NewGuid().ToString();
            else
            {
                object threadObjectContext = null;
                lock (_threadObjectContexts.SyncRoot)
                {
                    threadObjectContext = _threadObjectContexts[BuildContextThreadName()];
                }
                if (threadObjectContext != null)
                    objectContext = (ObjectContext)threadObjectContext;
            }
            return objectContext;
        }

        private static void StoreCurrentThreadObjectContext(ObjectContext objectContext)
        {
            lock (_threadObjectContexts.SyncRoot)
            {
                if (_threadObjectContexts.Contains(BuildContextThreadName()))
                    _threadObjectContexts[BuildContextThreadName()] = objectContext;
                else
                    _threadObjectContexts.Add(BuildContextThreadName(), objectContext);
            }
        }

        /// <summary>
        /// Elimina el contexto del hilo actual
        /// </summary>
        private static void RemoveCurrentThreadObjectContext()
        {
            lock (_threadObjectContexts.SyncRoot)
            {
                if (_threadObjectContexts.Contains(BuildContextThreadName()))
                {
                    ObjectContext objectContext = (ObjectContext)_threadObjectContexts[BuildContextThreadName()];
                    if (objectContext != null)
                    {
                        objectContext.Dispose();
                    }
                    _threadObjectContexts.Remove(BuildContextThreadName());
                }
            }
        }

        private static string BuildContextThreadName()
        {
            return Thread.CurrentThread.Name;
        }

        private static string BuildHttpContextName()
        {
            return OBJECT_CONTEXT_KEY;
        }

        #endregion

        #endregion
    }
}
