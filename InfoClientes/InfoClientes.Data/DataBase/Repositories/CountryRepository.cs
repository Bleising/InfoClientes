﻿using InfoClientes.Core.Entities;
using InfoClientes.Core.Interfaces;
using InfoClientes.Cross;
using InfoClientes.Cross.Resources;
using InfoClientes.Data.DataBase.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace InfoClientes.Data.DataBase.Repositories
{
    public class CountryRepository : GenericRepository<Pais>, ICountryRepository
    {
        private static LogErrGeneric _logsRegistro = new LogErrGeneric();
        public CountryRepository()
        {
            _logsRegistro.openTransactionFile(CrossResource.PathLog, CrossResource.FolderLog);
        }
        public IEnumerable<Country> GetAllCountries()
        {
            try
            {
                var CountryList = new List<Country>();
                Country ojCountry;
                foreach (var item in this.GetAll())
                {
                    ojCountry = new Country();
                    MaperModelEntityToCoreEntity(item, ojCountry);
                    CountryList.Add(ojCountry);
                }

                return CountryList;
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                //_logsRegistro.openTransactionFile(CrossResource.PathLog, CrossResource.FolderLog);
                _logsRegistro.appendIncomingTransactionUnit("InfoClientes", method.ToString(), "", ex.Message, 1, Environment.MachineName);
                throw ex;
            }
            finally
            {
                this.Terminate();
            }
        }

        private void MaperModelEntityToCoreEntity(Pais ModelEntity, Entity CoreEntity)
        {
            ((Country)CoreEntity).CountryID = ModelEntity.IDPais;
            ((Country)CoreEntity).NameCountry = ModelEntity.NombrePais;
        }
    }
}
