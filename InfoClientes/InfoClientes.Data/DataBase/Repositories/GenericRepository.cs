﻿using InfoClientes.Core.Entities;
using InfoClientes.Data.DataBase.EF.Context;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace InfoClientes.Data.DataBase.Repositories
{
    public abstract class GenericRepository<T> where T : Entity
    {

        /// <summary>
        /// Crea un nuevo objeto tipo ObjectContext
        /// </summary>
        public virtual ObjectContext ObjectContext
        {
            get
            {
                return ObjectContextManager.GetInfoClientesObjectContext();
            }
        }

        /// <summary>
        /// Ejecuta una consulta sobre la entidad recibida
        /// </summary>
        /// <returns></returns>
        private IQueryable<T> GetQuery()
        {
            return this.ObjectContext.CreateObjectSet<T>();
        }

        /// <summary>
        /// Ejecuta una consulta sobre la entidad recibida utilizando un filtro
        /// </summary>
        /// <param name="filter">Filtro de consulta</param>
        /// <returns></returns>
        public IQueryable<T> GetFilter(Expression<Func<T, bool>> filter)
        {
            return this.ObjectContext.CreateObjectSet<T>().Where(filter);
        }

        public IQueryable<TResult> GetFilter<TResult>(Expression<Func<T, bool>> filter,
                                  Expression<Func<T, TResult>> resultColumns)
        {
            return this.ObjectContext.CreateObjectSet<T>().Where(filter).Select(resultColumns);
        }

        /// <summary>
        /// Retorna una lista de entidades
        /// </summary>
        /// <returns></returns>
        public List<T> GetAll()
        {
            return GetQuery().ToList();
        }

        /// <summary>
        /// Almacena en el repositorio una nueva instancia de una entidad
        /// </summary>
        /// <param name="entity">Instancia de la entidad a ser registrada</param>
        public void Add(T entity)
        {
            this.ObjectContext.AddObject(GetBaseType(typeof(T)).Name.ToString(), entity);
            this.ObjectContext.SaveChanges();
        }

        /// <summary>
        /// Modifica una instancia de una entidad en base de datos
        /// </summary>
        /// <param name="entity">Instancia de entidad a ser modificada</param>
        public void Update(T entity)
        {
            this.ObjectContext.DetectChanges();
            this.ObjectContext.SaveChanges();
        }

        /// <summary>
        /// Elimina una instancia de una entidad en base de datos
        /// </summary>
        /// <param name="entity">Instancia de entidad a ser modificada</param>
        public void Delete(T entity)
        {
            this.ObjectContext.DeleteObject(entity);
            this.ObjectContext.SaveChanges();
        }

        /// <summary>
        /// Elimina del repositorio una lista de instancias de una entidad
        /// </summary>
        /// <param name="entities">Lista de instancias a ser eliminadas</param>
        public void Delete(List<T> entities)
        {
            foreach (var item in entities)
            {
                this.ObjectContext.DeleteObject(item);
            }

            this.ObjectContext.SaveChanges();
        }

        /// <summary>
        /// Define el tipo de entidad del parametro recibido
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private static Type GetBaseType(Type type)
        {
            Type baseType = type.BaseType;
            if (baseType != null && baseType != typeof(Entity))
            {
                return GetBaseType(type.BaseType);
            }
            return type;
        }

        /// <summary>
        /// Libera recursos utilizados
        /// </summary>
        public void Terminate()
        {
            ObjectContextManager.SetRepositoryContext(null);
        }
    }
}
