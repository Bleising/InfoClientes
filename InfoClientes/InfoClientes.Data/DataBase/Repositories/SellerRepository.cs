﻿using InfoClientes.Core.Entities;
using InfoClientes.Core.Interfaces;
using InfoClientes.Cross;
using InfoClientes.Cross.Resources;
using InfoClientes.Data.DataBase.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace InfoClientes.Data.DataBase.Repositories
{
    public class SellerRepository : GenericRepository<Vendedor>, ISellerRepository
    {
        private static LogErrGeneric _logsRegistro = new LogErrGeneric();
        public SellerRepository()
        {
            _logsRegistro.openTransactionFile(CrossResource.PathLog, CrossResource.FolderLog);
        }

        public IEnumerable<Seller> GetAllSellers()
        {
            try
            {
                var SellerList = new List<Seller>();
                Seller objSeller;
                foreach (var item in this.GetAll())
                {
                    objSeller = new Seller();
                    MaperModelEntityToCoreEntity(item, objSeller);
                    SellerList.Add(objSeller);
                }

                return SellerList;
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                //_logsRegistro.openTransactionFile(CrossResource.PathLog, CrossResource.FolderLog);
                _logsRegistro.appendIncomingTransactionUnit("InfoClientes", method.ToString(), "", ex.Message, 1, Environment.MachineName);
                throw ex;
            }
            finally
            {
                this.Terminate();
            }
        }


        public bool ExistSellerID(int SellerID)
        {
            try
            {
                var Cont = this.GetFilter(x => x.IDVendedor == SellerID).ToList().Count();
                if (Cont > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                //_logsRegistro.openTransactionFile(CrossResource.PathLog, CrossResource.FolderLog);
                _logsRegistro.appendIncomingTransactionUnit("InfoClientes", method.ToString(), "", ex.Message, 1, Environment.MachineName);
                throw ex;
            }
            finally
            {
                this.Terminate();
            }
        }

        private void MaperModelEntityToCoreEntity(Vendedor ModelEntity, Entity CoreEntity)
        {
            ((Seller)CoreEntity).SellerID = ModelEntity.IDVendedor;
            ((Seller)CoreEntity).Name = ModelEntity.NombreVendedor;
            ((Seller)CoreEntity).Identification = ModelEntity.Identificacion;
        }
    }
}
