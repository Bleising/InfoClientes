﻿using InfoClientes.Core.Entities;
using InfoClientes.Core.Interfaces;
using InfoClientes.Cross;
using InfoClientes.Cross.Resources;
using InfoClientes.Data.DataBase.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace InfoClientes.Data.DataBase.Repositories
{
    public class DepartamentRepository : GenericRepository<Departamento>, IDepartamentRepository
    {
        private static LogErrGeneric _logsRegistro = new LogErrGeneric();
        public DepartamentRepository()
        {
            _logsRegistro.openTransactionFile(CrossResource.PathLog, CrossResource.FolderLog);
        }
        public IEnumerable<Department> GetAllDepartaments(int CountryID)
        {
            try
            {
                var DepartamentList = new List<Department>();
                Department objDepartament;
                foreach (var item in this.GetFilter(x => x.PaisID == CountryID).ToList())
                {
                    objDepartament = new Department();
                    MaperModelEntityToCoreEntity(item, objDepartament);
                    DepartamentList.Add(objDepartament);
                }

                return DepartamentList;
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                //_logsRegistro.openTransactionFile(CrossResource.PathLog, CrossResource.FolderLog);
                _logsRegistro.appendIncomingTransactionUnit("InfoClientes", method.ToString(), "", ex.Message, 1, Environment.MachineName);
                throw ex;
            }
            finally
            {
                this.Terminate();
            }
        }

        private void MaperModelEntityToCoreEntity(Departamento ModelEntity, Entity CoreEntity)
        {
            ((Department)CoreEntity).DepartmentID = ModelEntity.IDDepartamento;
            ((Department)CoreEntity).NameDepartment = ModelEntity.NombreDepartamento;
            if (ModelEntity.Pais != null)
            {
                ((Department)CoreEntity).Country.CountryID = ModelEntity.Pais.IDPais;
                ((Department)CoreEntity).Country.NameCountry = ModelEntity.Pais.NombrePais;
            }
        }
    }
}
