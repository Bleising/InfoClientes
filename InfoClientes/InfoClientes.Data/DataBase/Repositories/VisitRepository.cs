﻿using InfoClientes.Core.Entities;
using InfoClientes.Core.Interfaces;
using InfoClientes.Cross;
using InfoClientes.Cross.Resources;
using InfoClientes.Data.DataBase.EF;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace InfoClientes.Data.DataBase.Repositories
{
    public class VisitRepository : GenericRepository<Visita>, IVisitRepository
    {
        private static LogErrGeneric _logsRegistro = new LogErrGeneric();
        public VisitRepository()
        {
            _logsRegistro.openTransactionFile(CrossResource.PathLog, CrossResource.FolderLog);
        }

        public IEnumerable<Visit> GetAllVisits()
        {
            try
            {
                var VisitList = new List<Visit>();
                Visit objVisit;
                foreach (var item in this.GetAll())
                {
                    objVisit = new Visit();
                    MaperModelEntityToCoreEntity(item, objVisit);
                    VisitList.Add(objVisit);
                }

                return VisitList;
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                //_logsRegistro.openTransactionFile(CrossResource.PathLog, CrossResource.FolderLog);
                _logsRegistro.appendIncomingTransactionUnit("InfoClientes", method.ToString(), "", ex.Message, 1, Environment.MachineName);
                throw ex;
            }
            finally
            {
                this.Terminate();
            }
        }

        public IEnumerable<Visit> GetVisitByClientID(int ClientID)
        {
            try
            {
                var VisitList = new List<Visit>();
                Visit objVisit;
                foreach (var item in this.GetFilter(x=> x.ClienteID == ClientID))
                {
                    objVisit = new Visit();
                    MaperModelEntityToCoreEntity(item, objVisit);
                    VisitList.Add(objVisit);
                }

                return VisitList;
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                //_logsRegistro.openTransactionFile(CrossResource.PathLog, CrossResource.FolderLog);
                _logsRegistro.appendIncomingTransactionUnit("InfoClientes", method.ToString(), "", ex.Message, 1, Environment.MachineName);
                throw ex;
            }
            finally
            {
                this.Terminate();
            }
        }

        public Response<bool> SaveVisit(Visit Visit)
        {
            var Response = new Response<bool>();
            try
            {
                var objVisita = new Visita()
                {
                    IDVisita = Visit.VisitID,
                    FechaVisita = DateTime.ParseExact(Visit.DateVisit, "dd/MM/yyyy", CultureInfo.InvariantCulture),
                    ValorNeto = Visit.NetWorth,
                    ValorVisita = Visit.ValueVisit,
                    Observaciones = Visit.Observations,
                    VendedorID = Visit.Seller.SellerID,
                    ClienteID = Visit.Client.ClientID
                };
                this.Add(objVisita);
                Response.Data = true;
                Response.Messages.Add("Datos guardados correctamente");
            }
            catch (Exception ex)
            {
                var message = ex.Message;
                var method = MethodBase.GetCurrentMethod();

                if (ex.InnerException != null)
                {
                    message = message + ". " + ex.InnerException.Message;
                }
                Response.Data = false;
                Response.Status = false;
                Response.Messages.Add("Error al guardar los datos de la visita");
                _logsRegistro.appendIncomingTransactionUnit("InfoClientes", method.Name, "", "Error al guardar los datos de la visita" + message + "  " + ex.InnerException, 500, method.ReflectedType.Name);
                throw ex;
            }
            finally
            {
                this.Terminate();
            }
            return Response;
        }

        public Response<bool> UpdateVisit(Visit Visit)
        {
            var Response = new Response<bool>();
            try
            {
                var Visita = this.GetFilter(x => x.IDVisita == Visit.VisitID).FirstOrDefault();

                if (Visita != null && !Visita.Equals(default(Visita)))
                {

                    Visita.IDVisita = Visit.VisitID;
                    Visita.FechaVisita = DateTime.ParseExact(Visit.DateVisit, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    Visita.ValorNeto = Visit.NetWorth;
                    Visita.ValorVisita = Visit.ValueVisit;
                    Visita.Observaciones = Visit.Observations;
                    Visita.VendedorID = Visit.Seller.SellerID;
                    Visita.ClienteID = Visit.Client.ClientID;
                    this.Update(Visita);
                    Response.Messages.Add("Datos actualizados correctamente");
                    Response.Data = true;
                }
                else
                {
                    Response.Messages.Add("La visita no existe en la base de datos");
                    Response.Data = false;
                    Response.Status = false;
                }
            }
            catch (Exception ex)
            {
                var message = ex.Message;
                var method = MethodBase.GetCurrentMethod();

                if (ex.InnerException != null)
                {
                    message = message + ". " + ex.InnerException.Message;
                }
                Response.Data = false;
                Response.Status = false;
                Response.Messages.Add("Error al actualizar los datos de la visita");
                _logsRegistro.appendIncomingTransactionUnit("InfoClientes", method.Name, "", "Error al actualizar los datos de la visita" + message + "  " + ex.InnerException, 500, method.ReflectedType.Name);
                throw ex;
            }
            finally
            {
                this.Terminate();
            }
            return Response;
        }

        public bool ExistVisitID(int VisitID)
        {
            try
            {
                var Cont = this.GetFilter(x => x.IDVisita == VisitID).ToList().Count();
                if (Cont > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                //_logsRegistro.openTransactionFile(CrossResource.PathLog, CrossResource.FolderLog);
                _logsRegistro.appendIncomingTransactionUnit("InfoClientes", method.ToString(), "", ex.Message, 1, Environment.MachineName);
                throw ex;
            }
            finally
            {
                this.Terminate();
            }
        }

        private void MaperModelEntityToCoreEntity(Visita ModelEntity, Entity CoreEntity)
        {
            ((Visit)CoreEntity).VisitID = ModelEntity.IDVisita;
            ((Visit)CoreEntity).DateVisit = Convert.ToString(ModelEntity.FechaVisita);
            ((Visit)CoreEntity).NetWorth = ModelEntity.ValorNeto;
            ((Visit)CoreEntity).ValueVisit = ModelEntity.ValorVisita;
            ((Visit)CoreEntity).Observations = ModelEntity.Observaciones;
            if (ModelEntity.Vendedor != null)
            {
                var Vendedor = ModelEntity.Vendedor;

                var objSeller = new Seller()
                {
                    SellerID = Vendedor.IDVendedor,
                    Name = Vendedor.NombreVendedor,
                    Identification = Vendedor.Identificacion
                };

                ((Visit)CoreEntity).Seller = objSeller;
            }
            if (ModelEntity.Cliente != null)
            {
                var Cliente = ModelEntity.Cliente;

                var objClient = new Client()
                {
                    ClientID = Cliente.IDCliente,
                    NIT = Cliente.Nit,
                    Name = Cliente.NombreCompleto,
                    Address = Cliente.Direccion,
                    Phone = Cliente.Telefono,
                    Quota = Cliente.Cupo,
                    BalanceQuota = Cliente.SaldoCupo,
                    PercentageVisit = Cliente.PorcentajeVisitas
                };
                if (Cliente.Ciudad != null)
                {
                    objClient.City.CityID = Cliente.Ciudad.IDCiudad;
                    objClient.City.NameCity = Cliente.Ciudad.NombreCiudad;
                }
                ((Visit)CoreEntity).Client = objClient;
            }
        }
    }
}
