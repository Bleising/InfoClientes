﻿using InfoClientes.Core.Entities;
using InfoClientes.Core.Interfaces;
using InfoClientes.Cross;
using InfoClientes.Cross.Resources;
using InfoClientes.Data.DataBase.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace InfoClientes.Data.DataBase.Repositories
{
    public class CityRepository : GenericRepository<Ciudad>, ICityRepository
    {

        private static LogErrGeneric _logsRegistro = new LogErrGeneric();
        public CityRepository()
        {
            _logsRegistro.openTransactionFile(CrossResource.PathLog, CrossResource.FolderLog);
        }
        public IEnumerable<City> GetAllCities(int DepartamentID)
        {
            try
            {
                var CityList = new List<City>();
                City objCity;
                foreach (var item in this.GetFilter(x => x.DepartamentoID == DepartamentID).ToList())
                {
                    objCity = new City();
                    MaperModelEntityToCoreEntity(item, objCity);
                    CityList.Add(objCity);
                }

                return CityList;
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                //_logsRegistro.openTransactionFile(CrossResource.PathLog, CrossResource.FolderLog);
                _logsRegistro.appendIncomingTransactionUnit("InfoClientes", method.ToString(), "", ex.Message, 1, Environment.MachineName);
                throw ex;
            }
            finally
            {
                this.Terminate();
            }
        }

        public bool ExistCityID(int CityID)
        {
            try
            {
                var Cont = this.GetFilter(x => x.IDCiudad == CityID).ToList().Count();
                if (Cont > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                //_logsRegistro.openTransactionFile(CrossResource.PathLog, CrossResource.FolderLog);
                _logsRegistro.appendIncomingTransactionUnit("InfoClientes", method.ToString(), "", ex.Message, 1, Environment.MachineName);
                throw ex;
            }
            finally
            {
                this.Terminate();
            }
        }

        private void MaperModelEntityToCoreEntity(Ciudad ModelEntity, Entity CoreEntity)
        {
            ((City)CoreEntity).CityID = ModelEntity.IDCiudad;
            ((City)CoreEntity).NameCity = ModelEntity.NombreCiudad;
            if (ModelEntity.Departamento != null)
            {
                ((City)CoreEntity).Department.DepartmentID = ModelEntity.Departamento.IDDepartamento;
                ((City)CoreEntity).Department.NameDepartment = ModelEntity.Departamento.NombreDepartamento;
            }
        }
    }
}
