﻿using InfoClientes.Core.Entities;
using InfoClientes.Core.Interfaces;
using InfoClientes.Cross;
using InfoClientes.Cross.Resources;
using InfoClientes.Data.DataBase.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace InfoClientes.Data.DataBase.Repositories
{
    public class ClientRepository : GenericRepository<Cliente>, IClientRepository
    {
        public ClientRepository()
        {
            _logsRegistro.openTransactionFile(CrossResource.PathLog, CrossResource.FolderLog);
        }
        private static LogErrGeneric _logsRegistro = new LogErrGeneric();
        public IEnumerable<Client> GetAllClients()
        {
            try
            {
                var ClientList = new List<Client>();
                Client ojClient;
                foreach (var item in this.GetFilter(x => x.Eliminado == false).ToList())
                {
                    ojClient = new Client();
                    MaperModelEntityToCoreEntity(item, ojClient);
                    ClientList.Add(ojClient);
                }

                return ClientList;
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                //_logsRegistro.openTransactionFile(CrossResource.PathLog, CrossResource.FolderLog);
                _logsRegistro.appendIncomingTransactionUnit("InfoClientes", method.ToString(), "", ex.Message, 1, Environment.MachineName);
                throw ex;
            }
            finally
            {
                this.Terminate();
            }
        }

        public Response<bool> SaveClient(Client Client)
        {
            var Response = new Response<bool>();
            try
            {
                var objCliente = new Cliente()
                {
                    Nit = Client.NIT,
                    NombreCompleto = Client.Name.Trim(),
                    Direccion = Client.Address.Trim(),
                    Telefono = Client.Phone.Trim(),
                    Cupo = Client.Quota,
                    SaldoCupo = Client.BalanceQuota,
                    PorcentajeVisitas = Client.PercentageVisit,
                    CiudadID = Client.City.CityID,
                    Eliminado = false
                };
                this.Add(objCliente);
                Response.Data = true;
                Response.Messages.Add("Datos guardados correctamente");
            }
            catch (Exception ex)
            {
                var message = ex.Message;
                var method = MethodBase.GetCurrentMethod();

                if (ex.InnerException != null)
                {
                    message = message + ". " + ex.InnerException.Message;
                }
                Response.Data = false;
                Response.Status = false;
                Response.Messages.Add("Error al guardar los datos del cliente");
                _logsRegistro.appendIncomingTransactionUnit("InfoClientes", method.Name, "", "Error al guardar los datos del cliente" + message + "  " + ex.InnerException, 500, method.ReflectedType.Name);
                throw ex;
            }
            finally
            {
                this.Terminate();
            }
            return Response;
        }


        public Response<bool> UpdateClient(Client Client)
        {
            var Response = new Response<bool>();
            try
            {
                var Cliente = this.GetFilter(x => x.IDCliente == Client.ClientID && x.Eliminado == false).FirstOrDefault();

                if (Cliente != null && !Cliente.Equals(default(Cliente)))
                {

                    Cliente.Nit = Client.NIT;
                    Cliente.NombreCompleto = Client.Name.Trim();
                    Cliente.Direccion = Client.Address.Trim();
                    Cliente.Telefono = Client.Phone.Trim();
                    Cliente.Cupo = Client.Quota;
                    Cliente.SaldoCupo = Client.BalanceQuota;
                    Cliente.PorcentajeVisitas = Client.PercentageVisit;
                    Cliente.CiudadID = Client.City.CityID;
                    this.Update(Cliente);
                    Response.Messages.Add("Datos actualizados correctamente");
                    Response.Data = true;
                }
                else
                {
                    Response.Messages.Add("El cliente no existe en la base de datos");
                    Response.Data = false;
                    Response.Status = false;
                }
            }
            catch (Exception ex)
            {
                var message = ex.Message;
                var method = MethodBase.GetCurrentMethod();

                if (ex.InnerException != null)
                {
                    message = message + ". " + ex.InnerException.Message;
                }
                Response.Data = false;
                Response.Status = false;
                Response.Messages.Add("Error al actualizar los datos del cliente");
                _logsRegistro.appendIncomingTransactionUnit("InfoClientes", method.Name, "", "Error al actualizar los datos del cliente" + message + "  " + ex.InnerException, 500, method.ReflectedType.Name);
                throw ex;
            }
            finally
            {
                this.Terminate();
            }
            return Response;
        }

        public Response<bool> DeleteClient(int ClientID)
        {
            var Response = new Response<bool>();
            try
            {
                var Cliente = this.GetFilter(x => x.IDCliente == ClientID && x.Eliminado == false).FirstOrDefault();

                if (Cliente != null && !Cliente.Equals(default(Cliente)))
                {

                    Cliente.Eliminado = true;

                    this.Update(Cliente);
                    Response.Messages.Add("Datos eliminados correctamente");
                    Response.Data = true;
                }
                else
                {
                    Response.Messages.Add("El cliente no existe en la base de datos");
                    Response.Data = false;
                    Response.Status = false;
                }
            }
            catch (Exception ex)
            {
                var message = ex.Message;
                var method = MethodBase.GetCurrentMethod();

                if (ex.InnerException != null)
                {
                    message = message + ". " + ex.InnerException.Message;
                }
                Response.Data = false;
                Response.Status = false;
                Response.Messages.Add("Error al eliminar los datos del cliente");
                _logsRegistro.appendIncomingTransactionUnit("InfoClientes", method.Name, "", "Error al eliminar los datos del cliente" + message + "  " + ex.InnerException, 500, method.ReflectedType.Name);
                throw ex;
            }
            finally
            {
                this.Terminate();
            }
            return Response;
        }

        public bool ExistClientNIT(string NIT,bool Deleted)
        {
            try
            {
                var Cont = this.GetFilter(x => x.Nit == NIT && x.Eliminado == Deleted).ToList().Count();
                if (Cont > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                //_logsRegistro.openTransactionFile(CrossResource.PathLog, CrossResource.FolderLog);
                _logsRegistro.appendIncomingTransactionUnit("InfoClientes", method.ToString(), "", ex.Message, 1, Environment.MachineName);
                throw ex;
            }
            finally
            {
                this.Terminate();
            }
        }


        public bool ExistClientID(int ClientID, bool Deleted)
        {
            try
            {
                var Cont = this.GetFilter(x => x.IDCliente == ClientID && x.Eliminado == Deleted).ToList().Count();
                if (Cont > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                //_logsRegistro.openTransactionFile(CrossResource.PathLog, CrossResource.FolderLog);
                _logsRegistro.appendIncomingTransactionUnit("InfoClientes", method.ToString(), "", ex.Message, 1, Environment.MachineName);
                throw ex;
            }
            finally
            {
                this.Terminate();
            }
        }

        private void MaperModelEntityToCoreEntity(Cliente ModelEntity, Entity CoreEntity)
        {
            ((Client)CoreEntity).ClientID = ModelEntity.IDCliente;
            ((Client)CoreEntity).NIT = ModelEntity.Nit;
            ((Client)CoreEntity).Name = ModelEntity.NombreCompleto;
            ((Client)CoreEntity).Phone = ModelEntity.Telefono;
            ((Client)CoreEntity).Address = ModelEntity.Direccion;
            ((Client)CoreEntity).Quota = ModelEntity.Cupo;
            ((Client)CoreEntity).BalanceQuota = ModelEntity.SaldoCupo;
            ((Client)CoreEntity).PercentageVisit = ModelEntity.PorcentajeVisitas;
            if(ModelEntity.Ciudad != null)
            {
                ((Client)CoreEntity).City.CityID = ModelEntity.Ciudad.IDCiudad;
                ((Client)CoreEntity).City.NameCity = ModelEntity.Ciudad.NombreCiudad;
            }
            if (ModelEntity.Ciudad.Departamento != null)
            {
                ((Client)CoreEntity).City.Department.DepartmentID = ModelEntity.Ciudad.Departamento.IDDepartamento;
                ((Client)CoreEntity).City.Department.NameDepartment = ModelEntity.Ciudad.Departamento.NombreDepartamento;
            }
            var CountryModel = ModelEntity.Ciudad.Departamento.Pais;
            var CountryCore = ((Client)CoreEntity).City.Department.Country;
            if (CountryModel != null)
            {
                CountryCore.CountryID = CountryModel.IDPais;
                CountryCore.NameCountry = CountryModel.NombrePais;

            }
        }
    }
}
