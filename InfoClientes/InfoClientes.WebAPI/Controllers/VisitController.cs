﻿using InfoClientes.Core.Domain;
using InfoClientes.Core.Entities;
using InfoClientes.Cross;
using InfoClientes.Cross.Resources;
using InfoClientes.Data.DataBase.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;
using System.Web.Http.Cors;

namespace InfoClientes.WebAPI.Controllers
{
    /// <summary>
    /// Controlador encargado de procesar la información de visitas
    /// </summary>
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("api/Visit")]
    public class VisitController : ApiController
    {
        private static LogErrGeneric _logsRegistro = new LogErrGeneric();

        /// <summary>
        /// Método encargado de guardar o actualizar una visita
        /// </summary>
        /// <param name="visit"></param>
        /// <returns></returns>
        [Route("PostSaveOrUpdateVisit/")]
        [HttpPost]
        public IHttpActionResult PostSaveOrUpdateVisit(Visit visit)
        {
            var result = new Response<bool>();
            try
            {
                if (visit != null)
                {
                    result = visit.Validate();
                    if (result.Status)
                    {
                        var Domain = BeginAction();
                        result = Domain.SaveOrUpdateVisit(visit);
                    }
                    return Ok(result);
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                _logsRegistro.openTransactionFile(CrossResource.PathLog, CrossResource.FolderLog);
                _logsRegistro.appendIncomingTransactionUnit("InfoClientes", method.ToString(), "", ex.Message, 1, Environment.MachineName);
                throw ex;
            }
        }

        /// <summary>
        /// Obtiene una lista de visitas por cliente
        /// </summary>
        /// <param name="clientID">ClientID a obtener visitas</param>
        /// <returns>Lista de visitas</returns>
        [Route("GetVistisByClientID/")]
        [HttpGet]
        public IHttpActionResult GetVistisByClientID(int clientID)
        {
            try
            {
                var Domain = BeginAction();
                return Ok(Domain.GetVisitByClientID(clientID));
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                _logsRegistro.openTransactionFile(CrossResource.PathLog, CrossResource.FolderLog);
                _logsRegistro.appendIncomingTransactionUnit("InfoClientes", method.ToString(), "", ex.Message, 1, Environment.MachineName);
                return NotFound();
            }
        }


        /// <summary>
        /// Obtiene una lista de visitas
        /// </summary>
        /// <returns>Lista de visitas</returns>
        [Route("GetAllVisits/")]
        [HttpGet]
        public IHttpActionResult GetAllVisits()
        {
            try
            {
                var Domain = BeginAction();
                return Ok(Domain.GetAllVisits());
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                _logsRegistro.openTransactionFile(CrossResource.PathLog, CrossResource.FolderLog);
                _logsRegistro.appendIncomingTransactionUnit("InfoClientes", method.ToString(), "", ex.Message, 1, Environment.MachineName);
                return NotFound();
            }
        }

        /// <summary>
        /// Inicia e inyecta los repositorios a las entidades de dominio
        /// </summary>
        /// <returns>Clase de dominio con el repositorio inyectado</returns>
        private DomVisit BeginAction()
        {
            var RepositoryClient = new ClientRepository();
            var RepositorySeller = new SellerRepository();
            var RepositoryVisit = new VisitRepository();
            var DomVisit = new DomVisit(RepositoryVisit, RepositoryClient, RepositorySeller);
            return DomVisit;
        }
    }
}
