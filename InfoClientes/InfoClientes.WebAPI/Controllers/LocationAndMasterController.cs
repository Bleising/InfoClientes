﻿using InfoClientes.Core.Domain;
using InfoClientes.Cross;
using InfoClientes.Cross.Resources;
using InfoClientes.Data.DataBase.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;
using System.Web.Http.Cors;

namespace InfoClientes.WebAPI.Controllers
{
    /// <summary>
    /// Controlador encargado de obtener todos los maestros y ubicaciones
    /// </summary>
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("api/LocationAndMaster")]
    public class LocationAndMasterController : ApiController
    {
        private static LogErrGeneric _logsRegistro = new LogErrGeneric();

        /// <summary>
        /// Método encargado de obtener la lista de paises
        /// </summary>
        /// <returns>Lista de paises</returns>
        [Route("GetAllCountries/")]
        [HttpGet]
        public IHttpActionResult GetAllCountries()
        {
            try
            {
                var Domain = BeginAction();
                return Ok(Domain.GetAllCountries());
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                _logsRegistro.openTransactionFile(CrossResource.PathLog, CrossResource.FolderLog);
                _logsRegistro.appendIncomingTransactionUnit("InfoClientes", method.ToString(), "", ex.Message, 1, Environment.MachineName);
                return NotFound();
            }
        }

        /// <summary>
        /// Obtiene todo los departamentos
        /// </summary>
        /// <param name="paisID">País al cual pertenecen los departamentos</param>
        /// <returns>Lista de departamentos</returns>
        [Route("GetAllDepartaments/")]
        [HttpGet]
        public IHttpActionResult GetAllDepartaments(int paisID)
        {
            try
            {
                var Domain = BeginAction();
                return Ok(Domain.GetAllDepartaments(paisID));
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                _logsRegistro.openTransactionFile(CrossResource.PathLog, CrossResource.FolderLog);
                _logsRegistro.appendIncomingTransactionUnit("InfoClientes", method.ToString(), "", ex.Message, 1, Environment.MachineName);
                return NotFound();
            }
        }

        /// <summary>
        /// Obtiene todas la ciudades
        /// </summary>
        /// <param name="departamentID">Departamento al cual pertenece la ciudad</param>
        /// <returns>Lista de ciudades</returns>
        [Route("GetAllCities/")]
        [HttpGet]
        public IHttpActionResult GetAllCities(int departamentID)
        {
            try
            {
                var Domain = BeginAction();
                return Ok(Domain.GetAllCities(departamentID));
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                _logsRegistro.openTransactionFile(CrossResource.PathLog, CrossResource.FolderLog);
                _logsRegistro.appendIncomingTransactionUnit("InfoClientes", method.ToString(), "", ex.Message, 1, Environment.MachineName);
                return NotFound();
            }
        }

        /// <summary>
        /// Obtiene todos los vendedores
        /// </summary>
        /// <returns>Lista de vendedores</returns>
        [Route("GetAllsellers/")]
        [HttpGet]
        public IHttpActionResult GetAllsellers()
        {
            try
            {
                var Domain = BeginAction();
                return Ok(Domain.GetAllSellers());
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                _logsRegistro.openTransactionFile(CrossResource.PathLog, CrossResource.FolderLog);
                _logsRegistro.appendIncomingTransactionUnit("InfoClientes", method.ToString(), "", ex.Message, 1, Environment.MachineName);
                return NotFound();
            }
        }



        /// <summary>
        /// Inicia e inyecta los repositorios a las entidades de dominio
        /// </summary>
        /// <returns>Clase de dominio con el repositorio inyectado</returns>
        private DomMasters BeginAction()
        {
            var RepositoryCountry = new CountryRepository();
            var RepositorySeller = new SellerRepository();
            var RepositoryDepartament = new DepartamentRepository();
            var RepositoryCity = new CityRepository();
            var DomMasters = new DomMasters(RepositoryCity, RepositoryDepartament, RepositoryCountry, RepositorySeller);
            return DomMasters;
        }
    }
}
