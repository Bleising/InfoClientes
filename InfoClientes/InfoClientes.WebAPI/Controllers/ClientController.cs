﻿using InfoClientes.Core.Domain;
using InfoClientes.Core.Entities;
using InfoClientes.Cross;
using InfoClientes.Cross.Resources;
using InfoClientes.Data.DataBase.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;
using System.Web.Http.Cors;

namespace InfoClientes.WebAPI.Controllers
{
    /// <summary>
    /// Controlador encargado de procesar la información del cliente
    /// </summary>
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("api/Client")]
    public class ClientController : ApiController
    {
        private static LogErrGeneric _logsRegistro = new LogErrGeneric();

        /// <summary>
        /// Guarda y actualiza un cliente. Para guardar se debe enviar el ClientID en cero (0)
        /// y para actualizar se debe enviar el ClientID a actualizar.
        /// </summary>
        /// <param name="client">Cliente a guardar o actualizar</param>
        /// <returns>Resultado de la operación</returns>
        [Route("PostSaveOrUpdateClient/")]
        [HttpPost]
        public IHttpActionResult PostSaveOrUpdateClient(Client client)
        {
            var result = new Response<bool>();
            try
            {
                if (client != null)
                {
                    result = client.Validate();
                    if (result.Status)
                    {
                        var Domain = BeginAction();
                        result = Domain.SaveOrUpdateClient(client);
                    }
                    return Ok(result);
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                _logsRegistro.openTransactionFile(CrossResource.PathLog, CrossResource.FolderLog);
                _logsRegistro.appendIncomingTransactionUnit("InfoClientes", method.ToString(), "", ex.Message, 1, Environment.MachineName);
                throw ex;
            }
        }

        /// <summary>
        /// Obtiene la lista de todos los clientes registrados
        /// </summary>
        /// <returns>Lista de clientes registrados</returns>
        [Route("GetAllClients/")]
        [HttpGet]
        public IHttpActionResult GetAllClients()
        {
            try
            {
                var Domain = BeginAction();
                return Ok(Domain.GetAllClients());
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                _logsRegistro.openTransactionFile(CrossResource.PathLog, CrossResource.FolderLog);
                _logsRegistro.appendIncomingTransactionUnit("InfoClientes", method.ToString(), "", ex.Message, 1, Environment.MachineName);
                return NotFound();
            }
        }

        /// <summary>
        /// Elimina un cliente según el ClientID
        /// </summary>
        /// <param name="ClientID">Identificador del cliente a eliminar</param>
        /// <returns>Estado de la operación</returns>
        [Route("DeleteClient/")]
        [HttpDelete]
        public IHttpActionResult DeleteClient(int ClientID)
        {
            try
            {
                var Domain = BeginAction();
                return Ok(Domain.DeleteClient(ClientID));
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                _logsRegistro.openTransactionFile(CrossResource.PathLog, CrossResource.FolderLog);
                _logsRegistro.appendIncomingTransactionUnit("InfoClientes", method.ToString(), "", ex.Message, 1, Environment.MachineName);
                return NotFound();
            }
        }

        /// <summary>
        /// Inicia e inyecta los repositorios a las entidades de dominio
        /// </summary>
        /// <returns>Clase de dominio con el repositorio inyectado</returns>
        private DomClient BeginAction()
        {
            var RepositoryClient = new ClientRepository();
            var RepositoryCity = new CityRepository();
            var DomClient = new DomClient(RepositoryClient, RepositoryCity);
            return DomClient;
        }
    }
}
