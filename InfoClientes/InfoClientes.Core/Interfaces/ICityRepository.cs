﻿using InfoClientes.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfoClientes.Core.Interfaces
{
    public interface ICityRepository
    {
        IEnumerable<City> GetAllCities(int DepartamentID);
        bool ExistCityID(int CityID);
    }
}
