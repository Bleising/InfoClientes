﻿using InfoClientes.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfoClientes.Core.Interfaces
{
    public interface IClientRepository
    {
        IEnumerable<Client> GetAllClients();
        Response<bool> SaveClient(Client Client);
        bool ExistClientNIT(string NIT, bool Deleted);
        bool ExistClientID(int ClientID, bool Deleted);
        Response<bool> UpdateClient(Client Client);
        Response<bool> DeleteClient(int ClientID);
    }
}
