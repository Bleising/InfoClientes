﻿using InfoClientes.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfoClientes.Core.Interfaces
{
    public interface IVisitRepository
    {
        IEnumerable<Visit> GetAllVisits();
        IEnumerable<Visit> GetVisitByClientID(int ClientID);
        Response<bool> SaveVisit(Visit Visit);
        Response<bool> UpdateVisit(Visit Visit);
        bool ExistVisitID(int VisitID);
    }
}
