﻿using InfoClientes.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfoClientes.Core.Interfaces
{
    public interface ICountryRepository
    {
        IEnumerable<Country> GetAllCountries();
    }
}
