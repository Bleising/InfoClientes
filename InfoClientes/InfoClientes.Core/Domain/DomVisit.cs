﻿using InfoClientes.Core.Entities;
using InfoClientes.Core.Interfaces;
using InfoClientes.Cross;
using InfoClientes.Cross.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace InfoClientes.Core.Domain
{
    public class DomVisit
    {
        #region Properties
        private IVisitRepository IVisitRepository;
        private IClientRepository IClientRepository;
        private ISellerRepository ISellerRepository;
        private static LogErrGeneric _logsRegistro = new LogErrGeneric();
        #endregion

        #region Constructors
        public DomVisit(IVisitRepository IVisitRepository, IClientRepository IClientRepository, ISellerRepository ISellerRepository)
        {
            this.IVisitRepository = IVisitRepository;
            this.IClientRepository = IClientRepository;
            this.ISellerRepository = ISellerRepository;
        }
        #endregion

        #region Public Methods
        public IEnumerable<Visit> GetAllVisits()
        {
            return this.IVisitRepository.GetAllVisits();
        }

        public IEnumerable<Visit> GetVisitByClientID(int ClientID)
        {
            return this.IVisitRepository.GetVisitByClientID(ClientID);
        }

        public Response<bool> SaveOrUpdateVisit(Visit Visit)
        {
            var Result = new Response<bool>();
            try
            {
                if (!this.IClientRepository.ExistClientID(Visit.Client.ClientID, false))
                {
                    Result.Messages.Add("El ClientID no existe");
                    Result.Status = false;
                }
                else if (!this.ISellerRepository.ExistSellerID(Visit.Seller.SellerID))
                {
                    Result.Messages.Add("El valor de la propiedad SellerID no existe");
                    Result.Status = false;
                }
                else
                {
                    if (Visit.VisitID == 0)
                    {
                        Result = this.IVisitRepository.SaveVisit(Visit);
                    }
                    else
                    {
                        if (this.IVisitRepository.ExistVisitID(Visit.VisitID))
                        {
                            Result = this.IVisitRepository.UpdateVisit(Visit);
                        }
                        else
                        {
                            Result.Messages.Add("El VisitID no existe");
                            Result.Status = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                _logsRegistro.openTransactionFile(CrossResource.PathLog, CrossResource.FolderLog);
                _logsRegistro.appendIncomingTransactionUnit("InfoClientes", $"{method.ToString()}", "", ex.Message, 1, Environment.MachineName);
                throw ex;
            }

            return Result;
        }
        #endregion
    }
}
