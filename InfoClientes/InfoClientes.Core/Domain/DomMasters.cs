﻿using InfoClientes.Core.Entities;
using InfoClientes.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfoClientes.Core.Domain
{
    public class DomMasters
    {
        private ICityRepository ICityRepository;
        private IDepartamentRepository IDepartamentRepository;
        private ICountryRepository ICountryRepository;
        private ISellerRepository ISellerRepository;
        public DomMasters(ICityRepository ICityRepository, IDepartamentRepository IDepartamentRepository,
                          ICountryRepository ICountryRepository, ISellerRepository ISellerRepository)
        {
            this.ICityRepository = ICityRepository;
            this.IDepartamentRepository = IDepartamentRepository;
            this.ICountryRepository = ICountryRepository;
            this.ISellerRepository = ISellerRepository;
        }

        public IEnumerable<City> GetAllCities(int DepartamentID)
        {
            return this.ICityRepository.GetAllCities(DepartamentID);
        }

        public IEnumerable<Department> GetAllDepartaments(int CountryID)
        {
            return this.IDepartamentRepository.GetAllDepartaments(CountryID);
        }

        public IEnumerable<Country> GetAllCountries()
        {
            return this.ICountryRepository.GetAllCountries();
        }

        public IEnumerable<Seller> GetAllSellers()
        {
            return this.ISellerRepository.GetAllSellers();
        }
    }
}
