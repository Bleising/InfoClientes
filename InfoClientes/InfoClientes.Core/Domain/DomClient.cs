﻿using InfoClientes.Core.Entities;
using InfoClientes.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sgi.Encrypter;
using InfoClientes.Cross;
using System.Reflection;
using InfoClientes.Cross.Resources;

namespace InfoClientes.Core.Domain
{
    public class DomClient
    {
        #region Properties
        private IClientRepository IClientRepository;
        private ICityRepository ICityRepository;
        private static LogErrGeneric _logsRegistro = new LogErrGeneric();
        #endregion

        #region Constructors
        public DomClient(IClientRepository IClientRepository, ICityRepository ICityRepository)
        {
            this.IClientRepository = IClientRepository;
            this.ICityRepository = ICityRepository;
        }
        #endregion

        #region PublicMethods

        public IEnumerable<Client> GetAllClients()
        {
            var ListClients = this.IClientRepository.GetAllClients();
            DecriptList(ref ListClients);
            return ListClients;
        }

        public Response<bool> DeleteClient(int ClientID)
        {
            return this.IClientRepository.DeleteClient(ClientID);
        }

        public Response<bool> SaveOrUpdateClient(Client Client)
        {
            var Result = new Response<bool>();
            Client.NIT = EncriptOrDesEncript(Client.NIT.Trim(), false);
            if (ExistClientNIT(Client.NIT, false))
            {
                Result.Messages.Add("El Nit ya existe");
                Result.Status = false;
            }
            else if (!ExistCityID(Client.City.CityID))
            {
                Result.Messages.Add("El valor de la propiedad CityID no existe");
                Result.Status = false;
            }
            else
            {
                if (Client.ClientID == 0)
                {
                    Result = this.IClientRepository.SaveClient(Client);
                }
                else
                {
                    if (ExistClientID(Client.ClientID, false))
                    {
                        Result = this.IClientRepository.UpdateClient(Client);
                    }
                    else
                    {
                        Result.Messages.Add("El ClientID no existe");
                        Result.Status = false;
                    }
                }

            }
            return Result;
        }

        #endregion

        #region PrivatedMethods
        private string EncriptOrDesEncript(string Text, bool DesEncript)
        {

            Encrypter Encript = new Encrypter();

            string Result = string.Empty;
            try
            {
                if (DesEncript)
                {
                    Result = Encript.DESDecrypt(Text);
                }
                else
                {
                    Result = Encript.DESEncrypt(Text);
                }
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                _logsRegistro.openTransactionFile(CrossResource.PathLog, CrossResource.FolderLog);
                _logsRegistro.appendIncomingTransactionUnit("InfoClientes", $"{method.ToString()} Error 2: {Encript.EncryptionErrorException().Message}", "", ex.Message, 1, Environment.MachineName);
                throw ex;

            }
            return Result;
        }

        private void DecriptList(ref IEnumerable<Client> ListClients)
        {
            try
            {
                foreach (var item in ListClients)
                {
                    item.NIT = EncriptOrDesEncript(item.NIT, true);
                }
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                _logsRegistro.openTransactionFile(CrossResource.PathLog, CrossResource.FolderLog);
                _logsRegistro.appendIncomingTransactionUnit("InfoClientes", $"{method.ToString()}", "", ex.Message, 1, Environment.MachineName);
                throw ex;
            }
        }
        private bool ExistClientNIT(string NIT, bool Deleted)
        {
            try
            {
                return this.IClientRepository.ExistClientNIT(NIT, Deleted);
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                _logsRegistro.openTransactionFile(CrossResource.PathLog, CrossResource.FolderLog);
                _logsRegistro.appendIncomingTransactionUnit("InfoClientes", $"{method.ToString()}", "", ex.Message, 1, Environment.MachineName);
                throw ex;
            }

        }

        private bool ExistCityID(int CityID)
        {
            try
            {
                return this.ICityRepository.ExistCityID(CityID);
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                _logsRegistro.openTransactionFile(CrossResource.PathLog, CrossResource.FolderLog);
                _logsRegistro.appendIncomingTransactionUnit("InfoClientes", $"{method.ToString()}", "", ex.Message, 1, Environment.MachineName);
                throw ex;
            }

        }

        private bool ExistClientID(int ClientID, bool Deleted)
        {
            try
            {
                return this.IClientRepository.ExistClientID(ClientID, Deleted);
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                _logsRegistro.openTransactionFile(CrossResource.PathLog, CrossResource.FolderLog);
                _logsRegistro.appendIncomingTransactionUnit("InfoClientes", $"{method.ToString()}", "", ex.Message, 1, Environment.MachineName);
                throw ex;
            }

        }
        #endregion

    }
}
