﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfoClientes.Core.Entities
{
    public class Department : Entity
    {
        public Department()
        {
            country = new Country();
        }
        private Country country;
        public int DepartmentID { get; set; }
        public string NameDepartment { get; set; }

        public Country Country
        {
            get
            {
                return country;
            }

            set
            {
                country = value;
            }
        }
    }
}
