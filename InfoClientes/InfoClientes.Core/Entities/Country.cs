﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfoClientes.Core.Entities
{
    public class Country : Entity
    {
        public int CountryID { get; set; }
        public string NameCountry { get; set; }
    }
}
