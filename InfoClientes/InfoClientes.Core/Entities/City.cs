﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfoClientes.Core.Entities
{
    public class City : Entity
    {
        public City()
        {
            department = new Department();
        }
        private Department department;
        public int CityID { get; set; }
        public string NameCity { get; set; }

        public Department Department
        {
            get
            {
                return department;
            }

            set
            {
                department = value;
            }
        }
    }
}
