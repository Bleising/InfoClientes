﻿using InfoClientes.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfoClientes.Core.Entities
{
    public class Visit : Entity, IValidateEntity
    {
        public Visit()
        {
            seller = new Seller();
            client = new Client();
        }
        private Seller seller;
        private Client client;
        public int VisitID { get; set; }
        public string DateVisit { get; set; }
        public decimal NetWorth { get; set; }
        public decimal ValueVisit { get; set; }
        public string Observations { get; set; }

        public Seller Seller
        {
            get
            {
                return seller;
            }

            set
            {
                seller = value;
            }
        }

        public Client Client
        {
            get
            {
                return client;
            }

            set
            {
                client = value;
            }
        }

        public Response<bool> Validate()
        {
            var Result = new Response<bool>();
            try
            {
                ValidateDecimal(VisitID, "VisitID", ref Result);
                ValidateDecimal(NetWorth, "NetWorth", ref Result);
                ValidateDecimal(ValueVisit, "ValueVisit", ref Result);
                ValidateString(Observations, "Observations", 500, ref Result);
                ValidateDecimal(Client.ClientID, "ClientID", ref Result);
                ValidateDecimal(Seller.SellerID, "SellerID", ref Result);
                if (Client.ClientID == 0)
                {
                    AddMessage(ref Result, "La propiedad ClientID no puede ser cero");
                }
                if (Seller.SellerID == 0)
                {
                    AddMessage(ref Result, "La propiedad SellerID no puede ser cero");
                }

                //validación de fecha
                DateTime DateVisitConvert = DateTime.MinValue;
                try
                {
                    if (string.IsNullOrEmpty(DateVisit))
                    {
                        AddMessage(ref Result, "El parámetro DateVisit es obligatorio");
                    }
                    else
                    {
                        DateVisitConvert = DateTime.ParseExact(DateVisit, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                        DateVisitConvert = DateVisitConvert.Date;
                        if (DateVisitConvert == DateTime.MinValue)
                        {
                            AddMessage(ref Result, "El parámetro DateVisit es obligatorio");
                        }
                        else if (DateVisitConvert < DateTime.Now.Date)
                        {
                            AddMessage(ref Result, "El parámetro DateVisit debe ser igual o superior a la fecha y hora actual");
                        }
                    }
                }
                catch (Exception)
                {
                    AddMessage(ref Result, "El parámetro DateVisit no tiene el formato correcto");
                }

            }
            catch (Exception)
            {

                throw;
            }
            return Result;
        }

        private void ValidateDecimal(decimal property, string textName, ref Response<bool> Result)
        {
            if (property < 0)
            {
                AddMessage(ref Result, $"El valor de la propiedad {textName} no puede ser negativo");
            }
        }

        private void ValidateString(string property, string textName, int length, ref Response<bool> Result)
        {
            if (string.IsNullOrEmpty(property))
            {
                AddMessage(ref Result, $"La propiedad {textName} es obligatorio");
            }
            else if (property.Length > length)
            {
                AddMessage(ref Result, $"La propiedad {textName} no puede contener más de {length} caracteres");
            }
        }

        private void AddMessage(ref Response<bool> Result, string message)
        {
            Result.Messages.Add(message);
            Result.Status = false;
        }
    }
}
