﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfoClientes.Core.Entities
{
    public class Response<T>
    {
        private List<string> messages;
        private bool status;

        public Response()
        {
            this.status = true;
            this.messages = new List<string>();
        }

        public T Data { get; set; }

        public List<string> Messages
        {
            get
            {
                return messages;
            }

            set
            {
                messages = value;
            }
        }

        public bool Status
        {
            get
            {
                return status;
            }

            set
            {
                status = value;
            }
        }
    }
}
