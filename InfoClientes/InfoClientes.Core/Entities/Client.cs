﻿using InfoClientes.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfoClientes.Core.Entities
{
    public class Client : Entity, IValidateEntity
    {
        public Client()
        {
            city = new City();
        }
        private City city;
        public int ClientID { get; set; }
        public string NIT { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public decimal Quota { get; set; }
        public decimal BalanceQuota { get; set; }
        public decimal PercentageVisit { get; set; }
        public City City
        {
            get { return city; }
            set { city = value; }
        }

        public Response<bool> Validate()
        {
            var Result = new Response<bool>();
            try
            {
                ValidateDecimal(ClientID, "ClientID", ref Result);
                ValidateString(NIT, "NIT", 100, ref Result);
                ValidateString(Name, "Name", 100, ref Result);
                ValidateString(Address, "Address", 200, ref Result);
                ValidateString(Phone, "Phone", 20, ref Result);
                ValidateDecimal(Quota, "Quota", ref Result);
                ValidateDecimal(BalanceQuota, "BalanceQuota", ref Result);
                ValidateDecimal(PercentageVisit, "PercentageVisit", ref Result);
                ValidateDecimal(City.CityID, "CityID", ref Result);
                if (City.CityID == 0)
                {
                    AddMessage(ref Result, "La propiedad CityID no puede ser cero");
                }

            }
            catch (Exception)
            {

                throw;
            }
            return Result;
        }

        private void ValidateDecimal(decimal property, string textName, ref Response<bool> Result)
        {
            if (property < 0)
            {
                AddMessage(ref Result, $"El valor de la propiedad {textName} no puede ser negativo");
            }
        }

        private void ValidateString(string property, string textName, int length, ref Response<bool> Result)
        {
            if (string.IsNullOrEmpty(property))
            {
                AddMessage(ref Result, $"La propiedad {textName} es obligatorio");
            }
            else if (property.Length > length)
            {
                AddMessage(ref Result, $"La propiedad {textName} no puede contener más de {length} caracteres");
            }
        }

        private void AddMessage(ref Response<bool> Result, string message)
        {
            Result.Messages.Add(message);
            Result.Status= false;
        }
    }
}
