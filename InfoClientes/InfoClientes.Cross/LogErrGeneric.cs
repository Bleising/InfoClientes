﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InfoClientes.Cross
{
    public class LogErrGeneric
    {
        private StreamWriter fileTransaction;
        public Queue messageBuffer;
        private Thread queueThread;
        public string ruta = String.Empty;

        public bool openTransactionFile(string path, string carpeta)
        {
            string nombreLog = "TraceInfoClintes";
            ruta = path + carpeta + @"\" + nombreLog + "_";
            CreateFolder(String.Format("{0}/{1}", path, carpeta));
            try
            {
                if (messageBuffer == null)
                    messageBuffer = new Queue();

                fileTransaction = new StreamWriter(ruta + DateTime.Now.ToString("yyyy-MM-dd") + ".log", true);

                queueThread = new Thread(new ThreadStart(fileWriterProcess));
                queueThread.Priority = ThreadPriority.BelowNormal;
                queueThread.Start();
                return true;
            }
            catch
            {
                return false;
            }
        }

        private void fileWriterProcess()
        {
            var hoy = DateTime.Now.Date;
            var hoy2 = DateTime.Now.Minute;

            string tmp;
            try
            {
                while (true)
                {
                    if (DateTime.Now.Date > hoy)
                    {
                        hoy = DateTime.Now.Date;

                        fileTransaction.Flush();
                        fileTransaction.Close();
                        fileTransaction = new StreamWriter(ruta + DateTime.Now.ToString("yyyy-MM-dd").Replace(":", "") + ".log", true);
                    }

                    lock (messageBuffer)
                    {
                        try
                        {
                            if (messageBuffer.Count > 0)
                            {
                                tmp = (string)messageBuffer.Dequeue();
                                if (tmp != null)
                                {
                                    lock (fileTransaction)
                                    {
                                        try
                                        {
                                            fileTransaction.WriteLine(tmp);
                                            fileTransaction.Flush();
                                        }
                                        catch
                                        {
                                            return;
                                        }
                                    }
                                }
                            }
                        }
                        catch
                        {

                        }

                    }
                    Thread.Sleep(1);
                }
            }
            catch (ThreadAbortException)
            {
                return;
            }
        }
        private void writeLineOnFile(string message)
        {
            lock (messageBuffer)
            {
                messageBuffer.Enqueue(message);
            }
        }

        /// <param name="message"></param>
        ///  /// //07/03/2016 HRM - HU325 -Pacific Rubiales - División servicios de windows
        /// Se modifica la declaracion del metodo agregando el parametro servidor.
        public void appendIncomingTransactionUnit(string usuario, string metodo, string claveRespuesta, string mensaje, int InOut, string Servidor)
        {
            try
            {
                writeLineOnFile(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + string.Format(" | {0} |\t{1}\t|\t{2}\t|\t{3}\t|\t{4}", usuario, metodo, InOut == 0 ? "OUT" : InOut == 1 ? "IN" : "ERROR", claveRespuesta, mensaje));
            }
            catch (Exception)
            {

            }
        }

        /// //07/03/2016 HRM - HU325 -Pacific Rubiales - División servicios de windows
        /// Se modifica la declaracion del metodo agregando el parametro servidor.
        public void appendIncomingTransactionUnit(string usuario, string metodo, string claveRespuesta, string mensaje, bool sendok, int numreintentos, long tiempo, string Servidor)
        {
            writeLineOnFile(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + string.Format(" | {0} |\t{1}\t|\t{2}\t|\t{3}\t|\t{4}\t|\t{5}\t|\t{6}\t|\t{7}", usuario, metodo, sendok ? "OK" : "ERROR", numreintentos, tiempo, claveRespuesta, mensaje, Servidor));
        }


        private void CreateFolder(string Path)
        {
            try
            {
                if (!(System.IO.Directory.Exists(Path)))
                {
                    System.IO.Directory.CreateDirectory(Path);
                }
            }
            catch
            { }
        }
    }
}
